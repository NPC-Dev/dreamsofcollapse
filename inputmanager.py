import pygame
from pubsub import pub
from component import commandqueue, position
from command import movecommand
from enum import Enum
from command import spelltype, castremovespell
from groundcollapse.tileset import Pos
import math
import musicmanager
from entities import entityutils

MAX_RANGE = 0

MAX_COMMAND_QUEUE_LEN = 1

class InputState(Enum):
    PLAYING_STANDARD = 1
    AREA_SELECTION = 2
    IN_MENU = 3

class InputManager:
    def __init__(self, levelViewer, world, physicsWorld, guiManager, tilesetData, chunkSize):
        self.world = world
        self.physicsWorld = physicsWorld
        self.guiManager = guiManager
        self.tilesetData = tilesetData
        self.tileSize = self.tilesetData.GetTileSize()
        self.chunkSize = chunkSize
        self.levelViewer = levelViewer

        self.playerEntity = None

        self.prevInputState = InputState.PLAYING_STANDARD
        self.inputState = InputState.PLAYING_STANDARD
        
        # Necessary for an annoying bug where we can't actually tell if the escape
        # key was already handled, so we would unpause and immediately pause again.
        self.unpausedThisFrame = False

        self.quitting = False

        pub.subscribe(self.OnPlayerSpawn, "player.spawn")
        pub.subscribe(self.OnEntityDied, "died")
        pub.subscribe(self.OnMenuEntered, "game.paused")
        pub.subscribe(self.OnUnpaused, "game.unpaused")
        pub.subscribe(self.OnUnpaused, "game.restart")
        pub.subscribe(self.OnUnpaused, "game.start")
        pub.subscribe(self.OnQuit, "game.quit")

    def OnMenuEntered(self):
        self.prevInputState = self.inputState
        self.inputState = InputState.IN_MENU
    
    def OnUnpaused(self):
        self.unpausedThisFrame = True
        self.inputState = self.prevInputState

    def OnEntityDied(self, entity, pos, dueToDamage=False):
        if entity == self.playerEntity:
            self.playerEntity = None

    def OnPlayerSpawn(self, player, pos):
        self.playerEntity = player.GetEntity()

    def ProcessInput(self, timeDiff):
        if self.quitting:
            return False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                self.quitting = True
                return False

            if self.ProcessGUI(event):
                continue
        
            if self.quitting:
                return False

            if event.type == pygame.KEYUP:
                self.ProcessKeyUp(event)
                continue

            if event.type == musicmanager.MusicManager.SONG_END_EVENT:
                pub.sendMessage("song.ended")

        self.ProcessHeldKeys(timeDiff)
        
        self.unpausedThisFrame = False
        return True

    def ProcessKeyUp(self, event):
        if self.inputState == InputState.PLAYING_STANDARD and \
            event.key == pygame.K_ESCAPE and \
            not self.unpausedThisFrame:
            self.levelViewer.ShowMainMenu()
            self.inputState = InputState.IN_MENU
            return True
        
        if self.playerEntity == None:
            return False
        
        if self.inputState == InputState.PLAYING_STANDARD and \
            event.key == pygame.K_u and \
            not self.unpausedThisFrame:
            self.levelViewer.ShowCharacterMenu()
            self.inputState = InputState.IN_MENU
            return True
        
        playerCommands = self.world.component_for_entity(self.playerEntity, commandqueue.CommandQueue)
        playerPosition = self.world.component_for_entity(self.playerEntity, position.Position)
        if self.playerEntity != None:
            if self.inputState == InputState.PLAYING_STANDARD:
                if event.key == pygame.K_a:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(-1, 0)))
                elif event.key == pygame.K_d:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(1, 0)))
                elif event.key == pygame.K_w:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(0, -1)))
                elif event.key == pygame.K_x:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(0, 1)))
                elif event.key == pygame.K_q:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(-1, -1)))
                elif event.key == pygame.K_e:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(1, -1)))
                elif event.key == pygame.K_c:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(1, 1)))
                elif event.key == pygame.K_z:
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(-1, 1)))
                elif event.key == pygame.K_s:
                    # TODO: Is moveCommand(0,0) a good way to do "stay still"?
                    self.AppendCommand(playerCommands,movecommand.MoveCommand(self.levelViewer, self.world, self.physicsWorld, pygame.Vector2(0, 0)))
                elif event.key == pygame.K_r:
                    if not entityutils.HasClarityRequiredForArea(self.playerEntity, self.world, pygame.Vector2(1,1)):
                        # TODO: More visual or audio feedback
                        print("Not enough clarity.")
                    else:
                        pub.sendMessage("areaselect.start", pos=playerPosition, spellType=spelltype.SpellType.REMOVE)
                        self.selection = pygame.Vector2(math.floor(math.floor(playerPosition.x / self.tileSize.x) / self.chunkSize.x), \
                                                        math.floor(math.floor(playerPosition.y / self.tileSize.y) / self.chunkSize.y))
                        self.startPos = pygame.Vector2(self.selection.x, self.selection.y)
                        self.selectionSize = pygame.Vector2(1,1)
                        self.inputState = InputState.AREA_SELECTION

            elif self.inputState == InputState.AREA_SELECTION:
                selectionMoveDir = None
                if event.key == pygame.K_a:
                    selectionMoveDir = Pos(-1, 0)
                elif event.key == pygame.K_d:
                    selectionMoveDir = Pos(1, 0)
                elif event.key == pygame.K_w:
                    selectionMoveDir = Pos(0, -1)
                elif event.key == pygame.K_x:
                    selectionMoveDir = Pos(0, 1)
                elif event.key == pygame.K_q:
                    selectionMoveDir = Pos(-1, -1)
                elif event.key == pygame.K_e:
                    selectionMoveDir = Pos(1, -1)
                elif event.key == pygame.K_c:
                    selectionMoveDir = Pos(1, 1)
                elif event.key == pygame.K_z:
                    selectionMoveDir = Pos(-1, 1)
                elif event.key == pygame.K_s:
                    pub.sendMessage("areaselect.set")
                    areaSelected = pygame.Rect(self.selection.x, self.selection.y, self.selectionSize.x, self.selectionSize.y)
                    self.AppendCommand(playerCommands,castremovespell.CastRemoveSpell(self.world, self.playerEntity, areaSelected, self.levelViewer, self.chunkSize, self.tileSize))
                    self.inputState = InputState.PLAYING_STANDARD
                elif event.key == pygame.K_r or event.key == pygame.K_ESCAPE:
                    pub.sendMessage("areaselect.cancel")
                    self.inputState = InputState.PLAYING_STANDARD
                elif event.key == pygame.K_EQUALS or \
                     event.key == pygame.K_KP_PLUS:
                    if not entityutils.HasClarityRequiredForArea(self.playerEntity, self.world, self.selectionSize+pygame.Vector2(1,1)):
                        # TODO: have more audio or visual feedback
                        print("Not enough clarity to increase size...")
                    else:
                        self.selectionSize.x += 1
                        self.selectionSize.y += 1
                        pub.sendMessage("areaselect.resize", size=self.selectionSize)
                elif event.key == pygame.K_MINUS or \
                    event.key == pygame.K_KP_MINUS:
                    self.selectionSize.x -= 1
                    self.selectionSize.y -= 1
                    if (self.selectionSize.x < 1):
                        self.selectionSize.x = 1
                    if (self.selectionSize.y < 1):
                        self.selectionSize.y = 1
                    pub.sendMessage("areaselect.resize", size=self.selectionSize)
                    # Check if selection is now outside the max range, move it.
                    if self.MoveSelection(pygame.Vector2(0,0)):
                        pub.sendMessage("areaselect.move", pos=self.selection)
                
                if selectionMoveDir != None and self.MoveSelection(selectionMoveDir):
                    pub.sendMessage("areaselect.move", pos=self.selection)
   
    def AppendCommand(self, commandQueue, newCommand):
        if len(commandQueue.commands) >= MAX_COMMAND_QUEUE_LEN:
            return False
        
        commandQueue.commands.append(newCommand)
        return True

    def MoveSelection(self, pos):
        minX = self.startPos.x - MAX_RANGE - self.selectionSize.x
        maxX = self.startPos.x + MAX_RANGE + self.selectionSize.x + 1
        minY = self.startPos.y - MAX_RANGE - self.selectionSize.y
        maxY = self.startPos.y + MAX_RANGE + self.selectionSize.y + 1
        newMinPos = pygame.Vector2(self.selection.x + pos.x, self.selection.y + pos.y)
        newMaxPos = pygame.Vector2(self.selection.x + pos.x + self.selectionSize.x, 
                                    self.selection.y + pos.y + self.selectionSize.y)
        if newMinPos.x < minX:
            newMinPos.x = minX
            newMaxPos.x = minX + self.selectionSize.x
        
        if newMinPos.y < minY:
            newMinPos.y = minY
            newMaxPos.y = minY + self.selectionSize.y

        if newMaxPos.x > maxX:
            newMinPos.x = maxX - self.selectionSize.x
            newMaxPos.x = minX + self.selectionSize.x

        if newMaxPos.y > maxY:
            newMinPos.y = maxY - self.selectionSize.y
            newMaxPos.y = minY + self.selectionSize.y

        if self.selection.x == newMinPos.x and \
            self.selection.y == newMinPos.y:
            return False

        self.selection.x = newMinPos.x
        self.selection.y = newMinPos.y
        return True

    def ProcessGUI(self, event):
        # For some reason, process_events does not actually return whether the
        # event was processed, so this does not actually stop us from processing the
        # event again, sadly.
        if self.guiManager.process_events(event):
            return True

        return False
        
    def OnQuit(self):
        self.quitting = True
    
    def ProcessHeldKeys(self, timeDiff):
        heldKeys = pygame.key.get_pressed()
        # TODO: handle any held keys we need to handle here.
