from enum import Enum

class Stat:
    def __init__(self, value = 0):
        self.value = value

    def GetValue(self):
        return self.value

    def SetValue(self, value):
        self.value = value

    def AddValue(self, amount):
        self.value += amount

class StatType(Enum):
    DETERMINATION = 1
    CREATIVITY = 2
    WILLPOWER = 3
    WIT = 4
    LEVEL = 5
    POINTSAVAILABLE = 6

class Stats:
    def __init__(self, statsDict = {}):
        self.stats = statsDict
