class Resource:
    def __init__(self, baseVal, currVal = None):
        self.baseVal = baseVal
        self.maxVal = baseVal
        if currVal == None:
            self.val = baseVal
        else:
            self.val = currVal
