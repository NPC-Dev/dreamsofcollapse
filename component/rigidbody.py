import pymunk

class EntityBody(pymunk.Body):
    def __init__(self, entity, mass=0, moment=0, body_type=pymunk.Body.DYNAMIC):
        pymunk.Body.__init__(self, mass, moment, body_type)
        self.entity = entity

class RigidBody:
    def __init__(self, body, offset):
        self.body = body
        self.offset = offset
