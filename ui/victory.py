from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_text_box import UITextBox
from pygame_gui.elements.ui_button import UIButton
import pygame
from pubsub import pub

class Victory(UIPanel):
    def __init__(self, rect, manager, element_id="gameover"):
        UIPanel.__init__(self, relative_rect=rect, manager=manager, element_id=element_id)
        
        self.youWinLabel = UITextBox(relative_rect=pygame.Rect((int(self.rect.width / 2) - 350,
                                                 int(self.rect.height / 2) - 100),
                                                (700, 200)),
                                        html_text=str("""
<font face='Megrim-Regular' color='#FFFFFF' size=7>You feel fully refreshed!</font><br><br><font face='Megrim-Regular' color='#FFFFFF' size=6>Your nightmares are vanquished - you will have the energy to face another day.</font>"""),
                                        manager=self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self,
                                        wrap_to_height=True)  
        
        self.continueButton = UIButton(relative_rect=pygame.Rect((self.get_container().rect.x + 80, self.get_container().rect.height - 80),
                                                              (200, 40)),
                                    text='Continue This Dream',
                                    manager=self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self
                                    )

        self.quitButton = UIButton(relative_rect=pygame.Rect((self.get_container().rect.width - 280, self.get_container().rect.height - 80),
                                                              (200, 40)),
                                    text='Exit Game',
                                    manager=self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self
                                    )

    def process_event(self, event):
        processed_event = False
        if event.type == pygame.USEREVENT:
            if event.user_type == 'ui_button_pressed':
                if event.ui_element == self.quitButton:
                    pygame.quit()
                    processed_event = True
                
                if event.ui_element == self.continueButton:
                    pub.sendMessage("game.unpaused")
                    self.kill()
                    processed_event = True
        
        return processed_event
