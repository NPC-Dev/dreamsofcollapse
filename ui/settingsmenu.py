from pygame_gui.elements.ui_panel import UIPanel
import pygame
from pubsub import pub
from pygame_gui.elements.ui_text_box import UITextBox
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_button import UIButton
from pygame_gui.elements.ui_horizontal_slider import UIHorizontalSlider
from pygame_gui.elements.ui_label import UILabel
import gamesettings
import pygame_gui

LOGO_WIDTH = 800

BUTTON_PADDING_HEIGHT = 15

class SettingsMenu(UIPanel):
    def __init__(self, rect, manager, element_id="settingsmenu", gameInProgress = False, currSettings = None):
        UIPanel.__init__(self, relative_rect=rect, manager=manager, element_id=element_id)

        self.change_layer(2)

        pub.subscribe(self.OnSettingsLoaded, "settings.loaded")
        
        self.initialized = False
        
        if currSettings == None:
            pub.sendMessage("settings.retrieve")
        else:
            self.currSettings = currSettings

        self.gameInProgress = gameInProgress

        self.headerLabel = UILabel(pygame.Rect((int(self.rect.width / 2) - 250,
                                                 int(10)),
                                                (500, 50)),
                                    "Game Settings",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)

        self.returnButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, rect.height-80),
                                                                      (200, 60)),
                                            text='Done',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )

        mainVolume = self.currSettings.GetValue("mainvolume")
        if mainVolume == None:
            mainVolume = 100.0
        self.mainVolumeSlider = UIHorizontalSlider(pygame.Rect((int(self.rect.width / 2),
                                                           int(self.headerLabel.rect.bottom + 10)),
                                                          (240, 25)),
                                              mainVolume,
                                              (0.0, 100.0),
                                              self.ui_manager,
                                              container=self.get_container(),
                                              parent_element=self)

        self.mainVolumeValueLabel = UILabel(pygame.Rect((int(self.rect.width / 2) + 250,
                                                 int(self.mainVolumeSlider.relative_rect.y)),
                                                (40, 25)),
                                    str(int(self.mainVolumeSlider.get_current_value())),
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
         
        self.mainVolumeLabel = UILabel(pygame.Rect((int(self.rect.width / 2) - 250,
                                                 int(self.mainVolumeSlider.relative_rect.y)),
                                                (200, 25)),
                                    "Main Volume",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        musicVolume = self.currSettings.GetValue("musicvolume")
        if musicVolume == None:
            musicVolume = 100.0
        self.musicVolumeSlider = UIHorizontalSlider(pygame.Rect((int(self.rect.width / 2),
                                                           int(self.mainVolumeSlider.relative_rect.bottom) + BUTTON_PADDING_HEIGHT),
                                                          (240, 25)),
                                              musicVolume,
                                              (0.0, 100.0),
                                              self.ui_manager,
                                              container=self.get_container(),
                                              parent_element=self)

        self.musicVolumeValueLabel = UILabel(pygame.Rect((int(self.rect.width / 2) + 250,
                                                 int(self.musicVolumeSlider.relative_rect.y)),
                                                (40, 25)),
                                    str(int(self.musicVolumeSlider.get_current_value())),
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
         
        self.musicVolumeLabel = UILabel(pygame.Rect((int(self.rect.width / 2) - 250,
                                                 int(self.musicVolumeSlider.relative_rect.y)),
                                                (200, 25)),
                                    "Music Volume",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.controlsLabel = UITextBox(relative_rect=pygame.Rect(int(self.rect.width / 2) - 200,
                                                 int(self.musicVolumeSlider.relative_rect.bottom + BUTTON_PADDING_HEIGHT),
                                                400, 500),
                                        html_text=str("""
<font face='Megrim-Regular' color='#FFFFFF' size=6>Controls:</font><br><br>Move Northwest: Q<br>Move North: W<br>Move Northeast: E<br>Move West: A<br>Wait: S<br>Move East: D<br>Move Southwest: Z<br>Move South: X<br>Move Southeast: C<br>(Movement keys also attack.)<br><br>Begin Selecting Lucid Dream Area: R<br><br>(When In Lucid Dream Selection):<br>Re-Imagine Area: S<br>Move Area: Same as Movement Keys<br>Enlarge Area: = or +<br>Reduce Area: - or keypad -<br><br>Pause Menu: Escape<br>Character Menu: U"""),
                                        manager=self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self,
                                        wrap_to_height=True)  

        
        self.initialized = True

    def update(self, time_delta):
        if self.alive():
            if self.mainVolumeSlider.has_moved_recently:
                self.mainVolumeValueLabel.set_text(str(int(self.mainVolumeSlider.get_current_value())))
                self.currSettings.SetValue("mainvolume", self.mainVolumeSlider.get_current_value())
            
            if self.musicVolumeSlider.has_moved_recently:
                self.musicVolumeValueLabel.set_text(str(int(self.musicVolumeSlider.get_current_value())))
                self.currSettings.SetValue("musicvolume", self.musicVolumeSlider.get_current_value())

    def OnSettingsLoaded(self, settings):
        self.currSettings = settings
        
        if not self.initialized:
            return

        mainVolume = self.currSettings.GetValue("mainvolume")
        if mainVolume == None:
            mainVolume = 100
        self.mainVolumeSlider.set_current_value(mainVolume)

        musicVolume = self.currSettings.GetValue("musicvolume")
        if musicVolume == None:
            musicVolume = 100
        self.musicVolumeSlider.set_current_value(musicVolume)

    def process_event(self, event):
        processed_event = False
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_ESCAPE:
                pub.sendMessage("settings.save", settings=self.currSettings)
                self.kill()
                processed_event = True
        
        if event.type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.returnButton:
                pub.sendMessage("settings.save", settings=self.currSettings)
                self.kill()
                processed_event = True

        return processed_event