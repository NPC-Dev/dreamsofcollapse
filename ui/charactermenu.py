from pygame_gui.elements.ui_panel import UIPanel
import pygame
from pubsub import pub
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_button import UIButton
from component import stats
import pygame_gui

LABEL_PADDING_X = 20
LABEL_PADDING_Y = 20
LABEL_HEIGHT = 40
LEFT_PADDING = 25

class CharacterMenu(UIPanel):
    def __init__(self, rect, manager, player, world, element_id="charactermenu"):
        UIPanel.__init__(self, relative_rect=rect, manager=manager, element_id=element_id)

        self.player = player
        self.world = world

        self.closeButton = UIButton(relative_rect=pygame.Rect((self.get_container().rect.width-20, 0),
                                                              (20, 20)),
                                    text='╳',
                                    manager=self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self
                                    )

        self.headerLabel = UILabel(pygame.Rect((int(self.rect.width / 2) - 250,
                                                 int(20)),
                                                (500, 50)),
                                    "The Dreamer",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)

        self.levelLabel = UILabel(pygame.Rect((int(self.rect.width / 2) - 250,
                                                 int(self.headerLabel.relative_rect.bottom + LABEL_PADDING_Y)),
                                                (150, LABEL_HEIGHT)),
                                    "Slumber Level",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.pointsAvailLabel = UILabel(pygame.Rect((int(self.levelLabel.relative_rect.right + 100),
                                                 int(self.headerLabel.relative_rect.bottom + LABEL_PADDING_Y)),
                                                (150, LABEL_HEIGHT)),
                                    "Points To Spend",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)

        self.determinationLabel = UILabel(pygame.Rect((int(LEFT_PADDING),
                                                 int(self.levelLabel.relative_rect.bottom + LABEL_PADDING_Y)),
                                                (200, LABEL_HEIGHT)),
                                    "Determination (+Repose)",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.creativityLabel = UILabel(pygame.Rect((int(LEFT_PADDING),
                                                 int(self.determinationLabel.relative_rect.bottom + LABEL_PADDING_Y)),
                                                (200, LABEL_HEIGHT)),
                                    "Creativity (+Clarity)",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.willpowerLabel = UILabel(pygame.Rect((int(LEFT_PADDING),
                                                 int(self.creativityLabel.relative_rect.bottom + LABEL_PADDING_Y)),
                                                (200, LABEL_HEIGHT)),
                                    "Willpower (+Damage)",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.witLabel = UILabel(pygame.Rect((int(LEFT_PADDING),
                                                 int(self.willpowerLabel.relative_rect.bottom + LABEL_PADDING_Y)),
                                                (200, LABEL_HEIGHT)),
                                    "Wit (+Speed)",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)

        statsComponent = self.world.try_component(self.player, stats.Stats)
        if statsComponent:
            determination = statsComponent.stats.get(stats.StatType.DETERMINATION, None)
            creativity = statsComponent.stats.get(stats.StatType.CREATIVITY, None)
            willpower = statsComponent.stats.get(stats.StatType.WILLPOWER, None)
            wit = statsComponent.stats.get(stats.StatType.WIT, None)
            level = statsComponent.stats.get(stats.StatType.LEVEL, None)
            pointsAvailable = statsComponent.stats.get(stats.StatType.POINTSAVAILABLE, None)
            
            self.levelAmount = UILabel(pygame.Rect((int(self.levelLabel.relative_rect.right + LABEL_PADDING_X),
                                                     int(self.levelLabel.relative_rect.y)),
                                                    (50, LABEL_HEIGHT)),
                                        str(level.GetValue()),
                                        self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self)
            
            self.pointsAvailableAmount = UILabel(pygame.Rect((int(self.pointsAvailLabel.relative_rect.right + LABEL_PADDING_X),
                                                     int(self.pointsAvailLabel.relative_rect.y)),
                                                    (50, LABEL_HEIGHT)),
                                        str(pointsAvailable.GetValue()),
                                        self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self)

            self.determinationAmount = UILabel(pygame.Rect((int(self.determinationLabel.relative_rect.right + LABEL_PADDING_X),
                                                     int(self.determinationLabel.relative_rect.y)),
                                                    (50, LABEL_HEIGHT)),
                                        str(determination.GetValue()),
                                        self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self)
            

            self.creativityAmount = UILabel(pygame.Rect((int(self.creativityLabel.relative_rect.right + LABEL_PADDING_X),
                                                     int(self.creativityLabel.relative_rect.y)),
                                                    (50, LABEL_HEIGHT)),
                                        str(creativity.GetValue()),
                                        self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self)
            
            self.willpowerAmount = UILabel(pygame.Rect((int(self.willpowerLabel.relative_rect.right + LABEL_PADDING_X),
                                                     int(self.willpowerLabel.relative_rect.y)),
                                                    (50, LABEL_HEIGHT)),
                                        str(willpower.GetValue()),
                                        self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self)
            
            self.witAmount = UILabel(pygame.Rect((int(self.witLabel.relative_rect.right + LABEL_PADDING_X),
                                                     int(self.witLabel.relative_rect.y)),
                                                    (50, LABEL_HEIGHT)),
                                        str(wit.GetValue()),
                                        self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self)

            self.CreateIncreaseButtons()

    def CreateIncreaseButtons(self):
        self.determinationIncreaseButton = UIButton(pygame.Rect((int(self.determinationAmount.relative_rect.right + LABEL_PADDING_X),
                                                 int(self.determinationLabel.relative_rect.y)),
                                                (50, LABEL_HEIGHT)),
                                    "+",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)

        self.creativityIncreaseButton = UIButton(pygame.Rect((int(self.creativityAmount.relative_rect.right + LABEL_PADDING_X),
                                                 int(self.creativityLabel.relative_rect.y)),
                                                (50, LABEL_HEIGHT)),
                                    "+",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.willpowerIncreaseButton = UIButton(pygame.Rect((int(self.willpowerAmount.relative_rect.right + LABEL_PADDING_X),
                                                 int(self.willpowerLabel.relative_rect.y)),
                                                (50, LABEL_HEIGHT)),
                                    "+",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)
        
        self.witIncreaseButton = UIButton(pygame.Rect((int(self.witAmount.relative_rect.right + LABEL_PADDING_X),
                                                 int(self.witLabel.relative_rect.y)),
                                                (50, LABEL_HEIGHT)),
                                    "+",
                                    self.ui_manager,
                                    container=self.get_container(),
                                    parent_element=self)

    def update(self, time_delta):
        if self.alive():
            if self.player == None:
                return

            statsComponent = self.world.try_component(self.player, stats.Stats)
            if statsComponent:
                determination = statsComponent.stats.get(stats.StatType.DETERMINATION, None)
                creativity = statsComponent.stats.get(stats.StatType.CREATIVITY, None)
                willpower = statsComponent.stats.get(stats.StatType.WILLPOWER, None)
                wit = statsComponent.stats.get(stats.StatType.WIT, None)
                pointsAvailable = statsComponent.stats.get(stats.StatType.POINTSAVAILABLE, None)
                
                if determination != None:
                    self.determinationAmount.set_text(str(determination.GetValue()))
                
                if creativity != None:
                    self.creativityAmount.set_text(str(creativity.GetValue()))
                
                if willpower != None:
                    self.willpowerAmount.set_text(str(willpower.GetValue()))
                
                if wit != None:
                    self.witAmount.set_text(str(wit.GetValue()))
               
                if pointsAvailable != None:
                    self.pointsAvailableAmount.set_text(str(pointsAvailable.GetValue()))

                    if pointsAvailable.GetValue() == 0:
                        self.determinationIncreaseButton.disable()
                        self.creativityIncreaseButton.disable()
                        self.willpowerIncreaseButton.disable()
                        self.witIncreaseButton.disable()

    def process_event(self, event):
        processed_event = False
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_ESCAPE or \
                event.key == pygame.K_u:
                pub.sendMessage("game.unpaused")
                self.kill()
                processed_event = True
        
        if event.type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.closeButton:
                pub.sendMessage("game.unpaused")
                self.kill()
                processed_event = True

            statsComponent = self.world.try_component(self.player, stats.Stats)
            if statsComponent:
                determination = statsComponent.stats.get(stats.StatType.DETERMINATION, None)
                creativity = statsComponent.stats.get(stats.StatType.CREATIVITY, None)
                willpower = statsComponent.stats.get(stats.StatType.WILLPOWER, None)
                wit = statsComponent.stats.get(stats.StatType.WIT, None)
                pointsAvailable = statsComponent.stats.get(stats.StatType.POINTSAVAILABLE, None)

                if event.ui_element == self.determinationIncreaseButton:
                    pointsAvailable.AddValue(-1)
                    determination.AddValue(1)
                elif event.ui_element == self.creativityIncreaseButton:
                    pointsAvailable.AddValue(-1)
                    creativity.AddValue(1)
                elif event.ui_element == self.willpowerIncreaseButton:
                    pointsAvailable.AddValue(-1)
                    willpower.AddValue(1)
                elif event.ui_element == self.witIncreaseButton:
                    pointsAvailable.AddValue(-1)
                    wit.AddValue(1)


        return processed_event
