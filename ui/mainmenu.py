from pygame_gui.elements.ui_panel import UIPanel
import pygame
from pubsub import pub
from pygame_gui.elements.ui_image import UIImage
from pygame_gui.elements.ui_button import UIButton
from ui import settingsmenu
import pygame_gui

LOGO_WIDTH = 800

BUTTON_PADDING_HEIGHT = 20

class MainMenu(UIPanel):
    def __init__(self, rect, manager, element_id="mainmenu", gameInProgress = False):
        UIPanel.__init__(self, relative_rect=rect, manager=manager, element_id=element_id)
        
        self.gameInProgress = gameInProgress

        self.logoImageSprite = pygame.image.load('sprites/npcdev/DreamsOfCollapseLogoTransparent.png').convert_alpha()

        logoScale = (LOGO_WIDTH/self.logoImageSprite.get_rect().width)
       
        self.logoImageSprite = pygame.transform.scale(self.logoImageSprite, (LOGO_WIDTH, int(self.logoImageSprite.get_rect().height * logoScale)))

        self.logoImage = UIImage(pygame.Rect((int(self.rect.width/2 - LOGO_WIDTH/2),
                                               int(20)),
                                              self.logoImageSprite.get_rect().size),
                                  self.logoImageSprite, self.ui_manager,
                                  container=self.get_container(),
                                  parent_element=self)

        self.startGameButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, rect.height/2),
                                                                      (200, 60)),
                                            text='Resume' if gameInProgress else 'Start',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )
        
        self.settingsButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, self.startGameButton.relative_rect.y + self.startGameButton.relative_rect.height + BUTTON_PADDING_HEIGHT),
                                                                      (200, 60)),
                                            text='Settings',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )

        self.endGameButton = UIButton(relative_rect=pygame.Rect((rect.width/2-100, self.settingsButton.relative_rect.y + self.settingsButton.relative_rect.height + BUTTON_PADDING_HEIGHT),
                                                                      (200, 60)),
                                            text='Quit',
                                            manager=self.ui_manager,
                                            container=self.get_container(),
                                            parent_element=self
                                        )

    def process_event(self, event):
        processed_event = False
        if event.type == pygame.KEYUP:
            if self.gameInProgress and event.key == pygame.K_ESCAPE:
                pub.sendMessage("game.start")
                self.kill()
                processed_event = True
                
        if event.type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.startGameButton:
                pub.sendMessage("game.start")
                self.kill()
                processed_event = True

            if event.ui_element == self.endGameButton:
                self.kill()
                pub.sendMessage("game.quit")
                pygame.quit()
                processed_event = True
            
            if event.ui_element == self.settingsButton:
                settingsmenu.SettingsMenu(self.rect, self.ui_manager, gameInProgress=self.gameInProgress)
                processed_event = True


        return processed_event
