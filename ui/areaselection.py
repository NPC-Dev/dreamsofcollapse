from pubsub import pub
import math
import pygame

SELECTION_COLOR = (170, 0, 30)
LINE_WIDTH = 2

class AreaSelection:
    def __init__(self, tileSize, chunkSize, scale, surface):
        self.surface = surface
        self.tileSize = tileSize
        self.chunkSize = chunkSize
        self.scale = scale
        self.selectionSize = pygame.Vector2(1,1)

        self.isActive = False

        pub.subscribe(self.OnStart, "areaselect.start")
        pub.subscribe(self.OnEnd, "areaselect.set")
        pub.subscribe(self.OnEnd, "areaselect.cancel")
        pub.subscribe(self.OnMove, "areaselect.move")
        pub.subscribe(self.OnResize, "areaselect.resize")

    def Draw(self, cameraPos, tickDiff):
        if not self.isActive:
            return
       
        width = (self.tileSize.x * self.chunkSize.x)*self.scale.x
        height = (self.tileSize.y * self.chunkSize.y)*self.scale.y

        rect = pygame.Rect(self.selection.x * width - cameraPos.x*self.scale.x,
                            self.selection.y * height - cameraPos.y*self.scale.y,
                            width * self.selectionSize.x,
                            height * self.selectionSize.y)

        pygame.draw.rect(self.surface, SELECTION_COLOR, rect, LINE_WIDTH)
 
    def OnStart(self, pos, spellType):
        self.selection = pygame.Vector2(math.floor(math.floor(pos.x / self.tileSize.x) / self.chunkSize.x), \
                                        math.floor(math.floor(pos.y / self.tileSize.y) / self.chunkSize.y))
        self.isActive = True
        self.startPos = pygame.Vector2(self.selection.x, self.selection.y)
        self.selectionSize = pygame.Vector2(1,1)

    def OnEnd(self):
        self.isActive = False

    def OnMove(self, pos):
        self.selection.x = pos.x
        self.selection.y = pos.y
    
    def OnResize(self, size):
        self.selectionSize.x = size.x
        self.selectionSize.y = size.y
