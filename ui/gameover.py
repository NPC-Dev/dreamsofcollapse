from pygame_gui.elements.ui_panel import UIPanel
from pygame_gui.elements.ui_text_box import UITextBox
import pygame
from pubsub import pub

class GameOver(UIPanel):
    def __init__(self, rect, manager, element_id="gameover"):
        UIPanel.__init__(self, relative_rect=rect, manager=manager, element_id=element_id)

        self.youDiedLabel = UITextBox(relative_rect=pygame.Rect((int(self.rect.width / 2) - 350,
                                                 int(self.rect.height / 2) - 100),
                                                (700, 200)),
                                        html_text=str("""
<font face='Megrim-Regular' color='#FFFFFF' size=7>You succumb to nightmare.</font><br><br><font face='Megrim-Regular' color='#FFFFFF' size=6>There will be no rejuvenating rest for you this night.<br>Press enter to collapse once more into slumber...</font>"""),
                                        manager=self.ui_manager,
                                        container=self.get_container(),
                                        parent_element=self,
                                        wrap_to_height=True)  

    def process_event(self, event):
        processed_event = False
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_RETURN:
                pub.sendMessage("game.restart")
                self.kill()
                processed_event = True
        
        return processed_event
