from pygame_gui.elements.ui_screen_space_health_bar import UIScreenSpaceHealthBar
from pygame_gui.core import UIContainer
from pygame_gui.elements.ui_label import UILabel
from pygame_gui.elements.ui_button import UIButton
import pygame
from component import repose
from component import refreshment
from component import clarity
from component import stats
from pubsub import pub
import pygame_gui

class ResourceSprite():
    def __init__(self):
        self.health_capacity = 100
        self.current_health = 0

class HUD(UIContainer):
    def __init__(self, rect, guiManager, element_id="hud"):
        UIContainer.__init__(self, relative_rect=rect, starting_height=1, manager=guiManager, object_id=element_id)

        self.reposeLabel = UILabel(pygame.Rect((20,10), (55, 25)),
                                    "Repose",
                                    self.ui_manager,
                                    container=None,
                                    parent_element=None)

        self.healthBar = UIScreenSpaceHealthBar(pygame.Rect((self.reposeLabel.rect.x, self.reposeLabel.rect.y + self.reposeLabel.rect.height), (200, 20)),
                                                 self.ui_manager,
                                                 container=None,
                                                 parent_element=None,
                                                 object_id="healthbar")
        self.reposeSprite = ResourceSprite()
        self.healthBar.set_sprite_to_monitor(self.reposeSprite)
        
        self.clarityLabel = UILabel(pygame.Rect((self.healthBar.rect.x + self.healthBar.rect.width + 10, 10), (65, 25)),
                                    "Clarity",
                                    self.ui_manager,
                                    container=None,
                                    parent_element=None)

        self.clarityBar = UIScreenSpaceHealthBar(pygame.Rect((self.clarityLabel.rect.x, self.clarityLabel.rect.y + self.clarityLabel.rect.height), (200, 20)),
                                                 self.ui_manager,
                                                 container=None,
                                                 parent_element=None,
                                                 object_id="claritybar")
        self.claritySprite = ResourceSprite()
        self.clarityBar.set_sprite_to_monitor(self.claritySprite)
        
        self.refreshmentLabel = UILabel(pygame.Rect((10,
                                                    self.rect.height - 120), (100, 25)),
                                    "Refreshment",
                                    self.ui_manager,
                                    container=None,
                                    parent_element=None)

        self.refreshmentBar = UIScreenSpaceHealthBar(pygame.Rect((self.refreshmentLabel.rect.x, self.refreshmentLabel.rect.y + self.refreshmentLabel.rect.height), (self.rect.width - self.refreshmentLabel.rect.x * 2, 20)),
                                                 self.ui_manager,
                                                 container=None,
                                                 parent_element=None,
                                                 object_id="refreshmentbar")
        self.refreshmentSprite = ResourceSprite()
        self.refreshmentBar.set_sprite_to_monitor(self.refreshmentSprite)
        
        self.characterButton = UIButton(relative_rect=pygame.Rect((50, self.rect.height-60),
                                                                      (120, 40)),
                                            text='Stats',
                                            manager=self.ui_manager,
                                            container=None,
                                            parent_element=None
                                        )
        self.levelUp = None

    def ShowLevelUp(self):
        if self.levelUp != None:
            return

        self.levelUp = UILabel(relative_rect=pygame.Rect((190, self.rect.height-60),
                                                                      (120, 40)),
                                            text='LEVEL UP!',
                                            manager=self.ui_manager,
                                            container=None,
                                            parent_element=None,
                                            object_id="levelup"
                                        )

    def KillLevelUp(self):
        if self.levelUp != None:
            self.levelUp.kill()

        self.levelUp = None

    def Update(self, world, player):
        reposeComponent = world.try_component(player, repose.Repose)
        if reposeComponent:
            self.reposeSprite.current_health = int(reposeComponent.val)
            self.reposeSprite.health_capacity = int(reposeComponent.maxVal)
        
        clarityComponent = world.try_component(player, clarity.Clarity)
        if clarityComponent:
            self.claritySprite.current_health = int(clarityComponent.val)
            self.claritySprite.health_capacity = int(clarityComponent.maxVal)
    
        refreshmentComponent = world.try_component(player, refreshment.Refreshment)
        if refreshmentComponent:
            self.refreshmentSprite.current_health = int(refreshmentComponent.val)
            self.refreshmentSprite.health_capacity = int(refreshmentComponent.maxVal)
    
        statsComp = world.try_component(player, stats.Stats)
        if statsComp:
            pointsAvailable = statsComp.stats.get(stats.StatType.POINTSAVAILABLE, None)
            if pointsAvailable != None and pointsAvailable.GetValue() > 0:
                self.ShowLevelUp()
            else:
                self.KillLevelUp()

    def process_event(self, event):
        processed_event = False
        
        if event.type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.characterButton:
                pub.sendMessage("menu.open", menu="character")
                processed_event = True

        return processed_event
