#! /usr/bin/python
import sys
import site
if not getattr(sys, 'frozen', False):
    site.addsitedir('groundcollapse/')

from groundcollapse import levelbuilder
from groundcollapse import tilesetloader
import pygame
import levelviewer
from groundcollapse import levelstore
from groundcollapse import tileset
from groundcollapse import buildqueue
from procgen import tilemetadata
import argparse
import threading

class QueueUpdaterThread(threading.Thread):
   
    def __init__(self, cameraRect, levelStore, chunkSize, worldBuildScreens):
        threading.Thread.__init__(self)
        self.cameraRect = cameraRect
        self.levelStore = levelStore
        self.chunkSize = chunkSize
        self.worldBuildScreens = worldBuildScreens
        self.chunkQueue = None

    def run(self):
        self.chunkQueue = self.UpdateChunkQueue()

    def UpdateChunkQueue(self):
        camRect = pygame.Rect(self.cameraRect)
        levelStore = self.levelStore
        chunkSize = self.chunkSize
        worldBuildScreens = self.worldBuildScreens
        
        chunkQueue = buildqueue.BuildQueue(self, self.levelStore, self.chunkSize)
        
        camRect.inflate_ip((worldBuildScreens-1) * camRect.width, (worldBuildScreens-1) * camRect.height)

        # Align camera to a chunk
        camRect.x = int(camRect.x / chunkSize.x)*chunkSize.x
        camRect.y = int(camRect.y / chunkSize.y)*chunkSize.y

        for x in range(camRect.x, camRect.x + camRect.width, int(chunkSize.x)):
            for y in range(camRect.y, camRect.y + camRect.height, int(chunkSize.y)):
                if levelStore.Contains(x, y):
                    continue
                
                chunkQueue.AddItem(tileset.Pos(x,y))
        
        chunkQueue.OnUpdatesFinished()
        return chunkQueue

    def GetCameraRect(self):
        result = self.cameraRect
        return result

    def GetCameraCenter(self):
        result = pygame.Vector2(self.cameraRect.x + self.cameraRect.width/2.0, self.cameraRect.y + self.cameraRect.height/2.0)
        return result

    def GetQueue(self):
        return self.chunkQueue

class QueueSorterThread(threading.Thread):
   
    def __init__(self, cameraRect, chunkQueue):
        threading.Thread.__init__(self)
        self.cameraRect = cameraRect
        self.chunkQueue = chunkQueue

    def run(self):
        self.UpdateChunkQueue()

    def UpdateChunkQueue(self): 
        self.chunkQueue.SetCameraHolder(self)
        self.chunkQueue.OnUpdatesFinished()

    def GetCameraRect(self):
        result = self.cameraRect
        return result

    def GetCameraCenter(self):
        result = pygame.Vector2(self.cameraRect.x + self.cameraRect.width/2.0, self.cameraRect.y + self.cameraRect.height/2.0)
        return result

    def GetQueue(self):
        return self.chunkQueue
        
class CameraHolder():
    def __init__(self, source=None):
        self.SetCameraSource(source)

    def SetCameraSource(self, source):
        self.cameraSource = source

    def GetCameraCenter(self):
        if self.cameraSource == None:
            return pygame.Vector2(0,0)
        else:
            return self.cameraSource.GetCameraCenter()

def ParseExtraTileData(extraData):
    return tilemetadata.TileMetadata(extraData)

def CreateBuilder():
    tilesetLoader = tilesetloader.TilesetLoader(tileExtraDataFactory=ParseExtraTileData)
    tiles = None
    if args.tileset != None and len(args.tileset) > 0:
        tiles = tilesetLoader.LoadTileset(args.tileset)

    return levelbuilder.LevelBuilder(tiles, args.path, args.program, seed = int(args.seed), levelSize = int(args.size), randFreq=args.rand_freq, numCores = args.cores, aggregateStats=args.aggregateStats)
    
def ResetBuilderForNewChunk(builder):
    return builder.Reset()

def ConstrainIfPlayerPresent(builder, pos, chunkSize, playerTilePos):
    playerPosInChunk = pygame.Vector2(playerTilePos.x - pos.x, playerTilePos.y - pos.y)

    if playerPosInChunk.x >= 0 and playerPosInChunk.x < chunkSize.x and \
        playerPosInChunk.y >= 0 and playerPosInChunk.y < chunkSize.y:
            builder.GenMustBeWalkableExternal(playerPosInChunk.x, playerPosInChunk.y)

    for x in range(-1,2):
        for y in range(-1,2):
            if x == 0 and y == 0:
                continue
            posInChunk = pygame.Vector2(playerPosInChunk.x + x, playerPosInChunk.y + y)
            if posInChunk.x >= 0 and posInChunk.x < chunkSize.x and \
                posInChunk.y >= 0 and posInChunk.y < chunkSize.y:
                
                # Generate the desire to be able to enter that tile.
                builder.GenDesiredExitExternal(playerPosInChunk.x, playerPosInChunk.y, x, y)
                
                # Generate the desire for that tile to have exits from us into it.
                builder.GenDesiredExitExternal(posInChunk.x, posInChunk.y, -x, -y)

def MakeChunkNeighborConstraints(levelStore, builder, pos, chunkSize):
    neighborChunkX = pos.x - chunkSize.x
    neighborChunk = levelStore.GetTiles(neighborChunkX, pos.y)
    if len(neighborChunk) > 0:
        for y in range(0, len(neighborChunk[0])):
            builder.GenPresetTileExternals(-1, y, neighborChunk[len(neighborChunk)-1][y])

    neighborChunkX = pos.x + chunkSize.x
    neighborChunk = levelStore.GetTiles(neighborChunkX, pos.y)
    if len(neighborChunk) > 0:
        for y in range(0, len(neighborChunk[0])):
            builder.GenPresetTileExternals(chunkSize.x, y, neighborChunk[0][y])
    
    neighborChunkY = pos.y - chunkSize.y
    neighborChunk = levelStore.GetTiles(pos.x, neighborChunkY)
    if len(neighborChunk) > 0:
        for x in range(0, len(neighborChunk)):
            builder.GenPresetTileExternals(x, -1, neighborChunk[x][len(neighborChunk[0])-1])

    neighborChunkY = pos.y + chunkSize.y
    neighborChunk = levelStore.GetTiles(pos.x, neighborChunkY)
    if len(neighborChunk) > 0:
        for x in range(0, len(neighborChunk)):
            builder.GenPresetTileExternals(x, chunkSize.y, neighborChunk[x][0])
    
    return True

def MakeChunkConstraints(levelViewer, levelStore, builder, pos, chunkSize):
    if not MakeChunkNeighborConstraints(levelStore, builder, pos, chunkSize):
        return False

    if levelViewer.playerEntity != None:
        playerPosition = levelViewer.GetPlayerTilePos()
        ConstrainIfPlayerPresent(builder, pos, chunkSize, playerPosition)
    return True

def UpdateChunkQueue(viewer, levelStore, chunkSize, worldBuildScreens):
    chunkQueueThread = QueueUpdaterThread(viewer.GetCameraRect(), levelStore, chunkSize, worldBuildScreens)
    chunkQueueThread.start()
    return chunkQueueThread

def SortChunkQueue(viewer, queue):
    chunkQueueThread = QueueSorterThread(viewer.GetCameraRect(), queue)
    chunkQueueThread.start()
    return chunkQueueThread

def ShouldAllowOptimizeTime(chunkRect, optimizationScreens, viewer):
    if optimizationScreens == 0:
        return True

    cameraRect = pygame.Rect(viewer.GetCameraRect())
    if optimizationScreens < 1:
        cameraRect.inflate_ip(-cameraRect.width * (1.0-optimizationScreens), -cameraRect.height * (1.0-optimizationScreens))
    else:
        cameraRect.inflate_ip(cameraRect.width * optimizationScreens, cameraRect.height * optimizationScreens) 

    return not cameraRect.colliderect(chunkRect)

if __name__ == '__main__':
    if getattr(sys, 'frozen', False):
        os.chdir(sys._MEIPASS)

    argparser = argparse.ArgumentParser()
    argparser.add_argument('--path', nargs="?", const="wfc.pl", type=str, default="groundcollapse/wfc.pl")
    argparser.add_argument('--program', nargs='?', const="main", type=str, default="main")
    argparser.add_argument('--tileset', nargs='?', const="", type=str, default="tilesets/calciumtrice-outdoor/simple-tileset-purple.json")
    argparser.add_argument('--seed', nargs='?', const=-1, type=int, default=-1)
    argparser.add_argument('--size', nargs='?', const=5, type=int, default=5)
    argparser.add_argument('--view', nargs='?', const=True, type=bool, default=True)
    argparser.add_argument('--display_width', nargs='?', const=850, type=int, default=850)
    argparser.add_argument('--display_height', nargs='?', const=850, type=int, default=850)
    argparser.add_argument('--scale_width', nargs='?', const=2, type=float, default=3)
    argparser.add_argument('--scale_height', nargs='?', const=2, type=float, default=3)
    argparser.add_argument('--rand_freq', nargs='?', const=1.0, type=float, default=1.0)
    argparser.add_argument('--cores', nargs='?', const=1, type=int, default=-1)
    argparser.add_argument('--failureMillis', nargs='?', const=500, type=int, default=500)
    argparser.add_argument('--fallbackOptimizeMillis', nargs='?', const=500, type=int, default=500)
    argparser.add_argument('--debugOnFallback', nargs='?', const=True, type=bool, default=False)
    argparser.add_argument('--worldBuildScreens', nargs='?', const=4.0, type=float, default=2.0)
    argparser.add_argument('--maxLoadedScreens', nargs='?', const=8.0, type=float, default=2.5)
    argparser.add_argument('--optimizationMillis', nargs='?', const=200, type=int, default=200)
    argparser.add_argument('--optimizationScreens', nargs='?', const=2.0, type=float, default=0)
    argparser.add_argument('--aggregateStats', nargs="?", const=False, type=bool, default=False)

    args = argparser.parse_args()

    builder = CreateBuilder()

    tileSize = builder.GetTileset().GetTileSize()
    screenSizeTilePixels = tileset.Pos(args.display_width/args.scale_width, args.display_height/args.scale_height)
    screenSizeTiles = tileset.Pos(screenSizeTilePixels.x/tileSize.x, screenSizeTilePixels.y/tileSize.y)
    screenSizeChunks = tileset.Pos(screenSizeTiles.x/args.size, screenSizeTiles.y/args.size)
    
    maxLoadedRegion = pygame.Vector2(screenSizeTiles.x*args.maxLoadedScreens, screenSizeTiles.y*args.maxLoadedScreens)
   
    cameraHolder = CameraHolder()
    levelStore = levelstore.LevelStore(maxLoadedRegion, cameraHolder)

    failureSeconds = args.failureMillis/1000.0
    fallbackOptimizeSeconds = args.fallbackOptimizeMillis/1000.0
    optimizationSeconds = args.optimizationMillis/1000.0

    pos = tileset.Pos(0,0)
    timeSinceBuildStart = 0.0
    timeSinceChunkQueueUpdate = 0.0
    timeSinceBuildEnd = 0.0

    chunkQueueThread = None

    if not args.view:
        builder.StartBuild()
    
    builderReady = True
    builderPrepped = False

    try:
        if args.view:
            
            done = False
            chunkSize = pygame.Vector2(args.size, args.size) 
            viewer = levelviewer.LevelViewer(levelStore, chunkSize, args.display_width, args.display_height, args.scale_width, args.scale_height, tilesetData = builder.GetTileset())
           
            cameraHolder.SetCameraSource(viewer)

            lastTick = pygame.time.get_ticks()
            
            if chunkQueueThread == None:
                chunkQueueThread = UpdateChunkQueue(viewer, levelStore, chunkSize, args.worldBuildScreens)
                    
            fallbackMode = False

            chunkQueue = buildqueue.BuildQueue(viewer, levelStore, chunkSize)

            while(viewer.Tick()):

                builder.Tick()
                
                newTime = pygame.time.get_ticks()
                tickDiff = (newTime - lastTick)/1000.0
                lastTick = newTime
            
                timeSinceChunkQueueUpdate += tickDiff

                resultLevel = builder.GetResultLevel()
               
                if chunkQueueThread != None and not chunkQueueThread.is_alive():
                    chunkQueue = chunkQueueThread.GetQueue()
                    chunkQueueThread = None
                elif timeSinceChunkQueueUpdate > 1.0 and chunkQueueThread == None:
                    timeSinceChunkQueueUpdate = 0.0
                    chunkQueueThread = UpdateChunkQueue(viewer, levelStore, chunkSize, args.worldBuildScreens)

                if not builderReady and builder.IsGrounded():
                    timeSinceBuildStart += tickDiff

                if None != resultLevel:
                    timeSinceBuildEnd += tickDiff
                    
                    if not builderReady and chunkQueue.Size() > 0:
                        if fallbackMode and timeSinceBuildEnd < fallbackOptimizeSeconds:
                            # Give fallback mode longer to refine.
                            continue
                        elif ShouldAllowOptimizeTime( \
                                pygame.Rect(pos.x,pos.y,len(resultLevel),len(resultLevel[0])), \
                                args.optimizationScreens, \
                                viewer) \
                                and timeSinceBuildEnd < optimizationSeconds:
                            # Give chunks generated offscreen a bit of time to optimize before finalizing them
                            continue

                        builder.Interrupt()
                        levelStore.StoreTiles(pos.x, pos.y, resultLevel)

                        if not ResetBuilderForNewChunk(builder):
                            continue
                        
                        if chunkQueueThread == None:
                            chunkQueueThread = SortChunkQueue(viewer, chunkQueue)

                        timeSinceBuildEnd = 0.0
                        builderReady = True
                        fallbackMode = False 
                elif not fallbackMode and not builderReady and timeSinceBuildStart > failureSeconds:
                    builder.Interrupt()
                    if not ResetBuilderForNewChunk(builder):
                        continue
                    
                    if args.debugOnFallback:
                        print("Build failed - entering debug mode: " + str(pos.x) + ", " + str(pos.y))
                        builder.SetDebugMode()
                    else:
                        print("Build failed - entering fallback mode: " + str(pos.x) + ", " + str(pos.y))
                        builder.SetFallbackMode()
                    fallbackMode = True
                    
                    if not MakeChunkConstraints(viewer, levelStore, builder, pos, tileset.Pos(args.size, args.size)):
                        continue

                    builderPrepped = True

                if builderReady and not builderPrepped and chunkQueueThread == None and chunkQueue.Size() > 0:
                    pos = chunkQueue.Pop()
                    while (levelStore.Contains(pos.x, pos.y)):
                        if chunkQueue.Size() == 0:
                            pos = None
                            break
                        pos = chunkQueue.Pop()

                    if pos == None:
                        continue

                    if not MakeChunkConstraints(viewer, levelStore, builder, pos, tileset.Pos(args.size, args.size)):
                        continue
                    
                    builderPrepped = True

                if builderPrepped and builder.StartBuild():
                    builderReady = False
                    builderPrepped = False
                    timeSinceBuildStart = 0.0

    finally:
        if args.aggregateStats:
            builder.PrintAvgStats()
 
