import pymunk
from pymunk.vec2d import Vec2d
import pymunk.pygame_util
import os

FRICTION = 0.3

class Masks():
    masks = \
    {
        "player":      0b01,
        "monster":       0b10,
        "tile":       0b100
    }

    collisionTypes = \
    {
        "player": 1,
        "monster": 2,
        "tile": 3
    }

    def GetCategory(name):
        return pymunk.ShapeFilter(categories=Masks.masks.get(name, 0b0))

    def GetDynamicPathBlockerFilter():
        return pymunk.ShapeFilter(mask=Masks.masks["player"] | Masks.masks["monster"])

    def GetMask(name):
        return pymunk.ShapeFilter(mask=pymunk.ShapeFilter.ALL_MASKS() ^ Masks.masks.get(name, 0b0))

    def GetCollisionType(name):
        return Masks.collisionTypes.get(name, 0)

class PhysicsWorld():
    def __init__(self, screen, threads=-1, iterations=5):
        if threads == -1:
            threads = os.cpu_count()-1

        self.space = pymunk.Space(threaded=(True if threads > 1 else False))
        self.space.threads = threads
        self.options = pymunk.pygame_util.DrawOptions(screen)
        pymunk.pygame_util.positive_y_is_up = False
        self.space.damping = FRICTION
        self.space.gravity = (0, 0)
        self.space.iterations = iterations

    def DebugDraw(self):
        self.space.debug_draw(self.options)

    def GetSpace(self):
        return self.space
