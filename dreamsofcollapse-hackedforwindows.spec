# -*- mode: python ; coding: utf-8 -*-

import pymunk

block_cipher = None

a = Analysis(['dreamsofcollapse.py'],
             pathex=['D:\\greywhind\\Code\\dreamsofcollapse', 'D:\\greywhind\\Code\\dreamsofcollapse\\groundcollapse'],
             binaries=[( pymunk.chipmunk_path, '.' )],
             datas=[('fonts','fonts'), ('tilesets','tilesets'), ('sprites','sprites'), ('sound', 'sound'), ('settings', 'settings'), ('README.md','.'), ('LICENSE.md','.'), ('groundcollapse/wfc.pl', 'groundcollapse'), ('groundcollapse/tiles', 'groundcollapse/tiles')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=['pygame'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
a.datas += Tree("D:/greywhind/Miniconda3/envs/dreams/Lib/site-packages/pygame", prefix= "pygame")
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='dreamsofcollapse',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='dreamsofcollapse')
