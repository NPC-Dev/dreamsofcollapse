import pandas
from pubsub import pub

SETTINGS_PATH = "settings/gamesettings.json"

class GameSettings:
    def __init__(self, settingsPath = None):
        if settingsPath == None:
            self.settingsPath = SETTINGS_PATH

        self.settingsFrame = pandas.DataFrame()
        self.settingsData = pandas.Series()
        self.settingsFrame["settings"] = self.settingsData

        self.TryLoadSettings()

    def TryLoadSettings(self):
        try:
            self.settingsFrame = pandas.read_json(self.settingsPath)
            self.settingsData = self.settingsFrame["settings"]
            print(str(self.settingsFrame))
       
            for key in self.settingsData.keys():
                pub.sendMessage("setting.updated", settings=self, key=key)
        except ValueError as e:
            print("No valid settings file found: " + str(e))
        except Exception as e:
            print("Could not load settings: " + str(e))

    def SaveSettings(self):
        saveFrame = pandas.DataFrame()
        saveFrame["settings"] = self.settingsData
        saveFrame.to_json(SETTINGS_PATH)

    def GetValue(self, key):
        return self.settingsData.get(key, None)
    
    def SetValue(self, key, value):
        self.settingsData[key] = value
        pub.sendMessage("setting.updated", settings=self, key=key)
