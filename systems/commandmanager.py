from esper import esper
from component import position, commandqueue, stats
import pygame
import sys
from pubsub import pub
import math

SPEED_BASE = 100
DECAY_RATE = 10
MIN_TIME_BETWEEN_TURNS = 0.15

class CommandManager(esper.Processor):
    def __init__(self):
        esper.Processor.__init__(self)
        self.mininitiative = 0
        self.timeelapsed = 0

    def process(self, deltaTime):
        self.timeelapsed += deltaTime
        if self.timeelapsed < MIN_TIME_BETWEEN_TURNS:
            return
            
        self.timeelapsed -= MIN_TIME_BETWEEN_TURNS

        newmininit = sys.maxsize

        for ent, (pos, queue) in self.world.get_components(position.Position, commandqueue.CommandQueue):
            if len(queue.commands) == 0:
                if queue.waitForMe:
                    newmininit = min(queue.initiative, newmininit)
                else:
                    queue.initiative = max(queue.initiative, self.mininitiative+1)
                continue

            if queue.initiative > self.mininitiative:
                newmininit = min(queue.initiative, newmininit)
                continue

            command = queue.commands[0]

            self.ProcessCommand(command, ent, pos)

            queue.commands.pop(0)
            queue.initiative += self.ComputeInitiativeChange(ent)
            newmininit = min(queue.initiative, newmininit)

        if (newmininit >= sys.maxsize):
            return
            
        if newmininit > self.mininitiative:
            pub.sendMessage("turns.passed", newTurn=newmininit, oldTurn=self.mininitiative)
            self.mininitiative = newmininit

    def ComputeInitiativeChange(self, ent):
        initiativeChange = SPEED_BASE
        statsComponent = self.world.try_component(ent, stats.Stats)
        if statsComponent:
            stat = statsComponent.stats.get(stats.StatType.WIT, None)
            if stat == None:
                return initiativeChange

            # Use an exponential decay to reduce initiative cost based on the stat
            initiativeChange *= 1.0/math.pow(((stat.GetValue()+DECAY_RATE-1)/DECAY_RATE), 2)

        return int(initiativeChange)

    def ProcessCommand(self, command, ent, pos):
        command.execute(ent)        
