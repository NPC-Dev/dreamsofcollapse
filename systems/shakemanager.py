from esper import esper
from pymunk import vec2d
from component import shake, rigidbody
import pygame
import math
import random

class ShakeManager(esper.Processor):
    def process(self, deltaTime):
        for ent, (physics, shakeData) in self.world.get_components(rigidbody.RigidBody, shake.Shake):
            vector = shakeData.vector
            if shakeData.vector == None:
                angle = random.uniform(0, 2.0*math.pi)
                vector = vec2d.Vec2d(shakeData.strength * math.cos(angle), shakeData.strength * math.sin(angle))
            else:
                vector = vector * shakeData.strength

            physics.body.apply_impulse_at_local_point((vector.x, vector.y))

            shakeData.time -= deltaTime
            if shakeData.time <= 0:
                self.world.remove_component(ent, shake.Shake)
