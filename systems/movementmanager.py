from esper import esper
from component import movementstate, rigidbody, position
import pygame
import sys
from pymunk import vec2d
from physics import physicsworld
import math
from pubsub import pub

maxVel = 1000
velMult = 10
minDistThreshold=0.001
minVelThreshold=0.00001

class MovementManager(esper.Processor):
    def __init__(self, physicsWorld, tileSize):
        esper.Processor.__init__(self)
        self.tileSize = tileSize

        #h = physicsWorld.GetSpace().add_collision_handler(
        #        physicsworld.Masks.GetCollisionType("monster"),
        #        physicsworld.Masks.GetCollisionType("monster"))
        #h.begin = self.UndoMoveGoal
    
    def UndoMoveGoal(self, arbiter, space, data):
        
        firstObj = arbiter.shapes[0].body.entity
        secondObj = arbiter.shapes[1].body.entity

        firstCurrPos = arbiter.shapes[0].body.position
        secondCurrPos = arbiter.shapes[1].body.position
        firstPosDiff = pygame.Vector2(0,0)
        secondPosDiff = pygame.Vector2(0,0)

        firstMoveState = self.world.try_component(firstObj, movementstate.MovementState)
        if firstMoveState:
            firstPosDiff = pygame.Vector2(firstCurrPos.x - firstMoveState.goalPos.x,
                                            firstCurrPos.y - firstMoveState.goalPos.y)
            
        secondMoveState = self.world.try_component(secondObj, movementstate.MovementState)
        if secondMoveState:
            secondPosDiff = pygame.Vector2(secondCurrPos.x - secondMoveState.goalPos.x,
                                            secondCurrPos.y - secondMoveState.goalPos.y)
       
        firstGoalDist = firstPosDiff.magnitude_squared()
        secondGoalDist = secondPosDiff.magnitude_squared()
        
        if firstGoalDist < secondGoalDist:
            pub.sendMessage("entity.move", entity=secondObj, oldPos=secondMoveState.goalPos, newPos=secondMoveState.lastGoodPos)
            secondMoveState.goalPos = secondMoveState.lastGoodPos
        else:
            pub.sendMessage("entity.move", entity=firstObj, oldPos=firstMoveState.goalPos, newPos=firstMoveState.lastGoodPos)
            firstMoveState.goalPos = firstMoveState.lastGoodPos

        return True

    def process(self, deltaTime):
        for ent, (moveState, physics, pos) in self.world.get_components(movementstate.MovementState, rigidbody.RigidBody, position.Position):
            if not moveState.goalPos:
                continue

            distToGoalX = moveState.goalPos.x - pos.x
            distToGoalY = moveState.goalPos.y - pos.y
            
            if (abs(distToGoalX) + abs(distToGoalY)) < minDistThreshold:
                moveState.lastGoodPos = moveState.goalPos
                continue

            desiredVelX = (1 if distToGoalX > 0 else -1) * min(maxVel, abs(distToGoalX*velMult))
            desiredVelY = (1 if distToGoalY > 0 else -1) * min(maxVel, abs(distToGoalY*velMult))

            if (abs(desiredVelX) + abs(desiredVelY)) < minVelThreshold:
                physics.body.velocity = vec2d.Vec2d(desiredVelX, desiredVelY)
                continue 
            
            newVelX = self.Interpolate(physics.body.velocity.x, desiredVelX, deltaTime)
            newVelY = self.Interpolate(physics.body.velocity.y, desiredVelY, deltaTime)
                
            physics.body.velocity = vec2d.Vec2d(newVelX, newVelY)

    def Interpolate(self, minVal, maxVal, t):
        return (t*minVal) + ((1-t) * maxVal)
