from esper import esper
from component import repose, stats

class ComputeStat(esper.Processor):
    def __init__(self):
        esper.Processor.__init__(self)

    def process(self, deltaTime):
        resourceComponentType = self.GetResourceComponent()
        statType = self.GetStatType()
        for ent, (resourceComp, statsComp) in self.world.get_components(resourceComponentType, stats.Stats):
            currentStat = statsComp.stats.get(statType, None)
            if currentStat == None:
                bonusResource = 0
            else:
                bonusResource = currentStat.GetValue()
            
            maxStat = self.ComputeMax(resourceComp.baseVal, bonusResource)
            valChange = maxStat - resourceComp.maxVal

            resourceComp.maxVal = maxStat
            if self.IncreaseCurrent():
                resourceComp.val += valChange
    
    def ComputeMax(self, baseVal, bonus):
        return baseVal + bonus

    def IncreaseCurrent(self):
        return True

    def GetResourceComponent(self):
        return repose.Repose

    def GetStatType(self):
        return stats.StatType.DETERMINATION
