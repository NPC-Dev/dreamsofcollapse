from esper import esper
from component import repose, rigidbody
import math
from pubsub import pub
from entities import entityutils

class Death(esper.Processor):
    def __init__(self, physicsWorld):
        esper.Processor.__init__(self)
        self.physicsWorld = physicsWorld
        

    def process(self, deltaTime):
        physicsToDelete = []
        for ent, reposeInfo in self.world.get_component(repose.Repose):
            if math.floor(reposeInfo.val) > 0:
                continue
                
            rigidBody = self.world.try_component(ent, rigidbody.RigidBody)
            if rigidBody:
                physicsToDelete.append(rigidBody.body)
                physicsToDelete.append(*rigidBody.body.shapes)

            self.world.delete_entity(ent)
            eventualPos = entityutils.GetEventualPosition(self.world, ent)
            pub.sendMessage("died", entity=ent, pos=eventualPos, dueToDamage=True)
            
        if len(physicsToDelete) > 0:
            self.physicsWorld.GetSpace().remove(*physicsToDelete)
