from systems import computestats
from component import refreshment, stats
import math

LEVEL_COST_BASE = 100
LEVEL_SCALING_EXPONENT = 2

class ComputeRefreshment(computestats.ComputeStat):
    def GetResourceComponent(self):
        return refreshment.Refreshment

    def GetStatType(self):
        return stats.StatType.LEVEL

    def IncreaseCurrent(self):
        return False

    def ComputeMax(self, baseVal, bonus):
        return baseVal + LEVEL_COST_BASE * math.pow((bonus-1), LEVEL_SCALING_EXPONENT)
