from systems import computestats

REPOSE_PER_DETERMINATION = 3

class ComputeRepose(computestats.ComputeStat):
    def ComputeMax(self, baseVal, bonus):
        return baseVal + bonus * REPOSE_PER_DETERMINATION
