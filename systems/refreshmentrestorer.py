from systems import resourcerestorer
from component import refreshment

REFRESHMENT_RESTORE_RATE = 0.01

class RefreshmentRestorer(resourcerestorer.ResourceRestorer):
    def GetBaseRestore(self):
        return REFRESHMENT_RESTORE_RATE

    def GetExtraComponents(self):
        return []

    def GetBonuses(self, ent, extraComponents):
        return 0

    def GetResourceComponentType(self):
        return refreshment.Refreshment
