from systems import computestats
from component import clarity, stats

CLARITY_PER_CREATIVITY = 2

class ComputeClarity(computestats.ComputeStat):
    def GetResourceComponent(self):
        return clarity.Clarity

    def ComputeMax(self, baseVal, bonus):
        return baseVal + bonus * CLARITY_PER_CREATIVITY

    def GetStatType(self):
        return stats.StatType.CREATIVITY
