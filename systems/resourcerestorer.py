from esper import esper
from component import resource
from pubsub import pub

class ResourceRestorer(esper.Processor):
    def __init__(self):
        esper.Processor.__init__(self)
        self.turnDelta = 0

        pub.subscribe(self.TurnsPassed, "turns.passed")

    def TurnsPassed(self, newTurn, oldTurn):
        self.turnDelta += newTurn - oldTurn

    def GetBaseRestore(self):
        return 1

    def GetExtraComponents(self):
        return []

    def GetBonuses(self, ent, extraComponents):
        return 0

    def GetResourceComponentType(self):
        return resource.Resource

    def process(self, deltaTime):
        resourceComponentType = self.GetResourceComponentType()
        extraComponents = self.GetExtraComponents()
        baseRestoreRate = self.GetBaseRestore()
        for ent, components in self.world.get_components(resourceComponentType, *extraComponents):
            resource = components[0]
            extraComponents = []
            if len(components) > 1:
                extraComponents = components[1:]

            restoreRate = baseRestoreRate + self.GetBonuses(ent, extraComponents)

            resource.val += max(0, min(resource.maxVal - resource.val, restoreRate * self.turnDelta))
        
        self.turnDelta = 0
