from systems import resourcerestorer
from component import repose, stats

REPOSE_RESTORE_RATE = 0.0008

HEAL_INCREASE_PER_DETERMINATION = 0.0005

class ReposeRestorer(resourcerestorer.ResourceRestorer):
    def GetBaseRestore(self):
        return REPOSE_RESTORE_RATE

    def GetExtraComponents(self):
        return [stats.Stats]

    def GetBonuses(self, ent, extraComponents):
        statsComponent = extraComponents[0]
        determination = statsComponent.stats.get(stats.StatType.DETERMINATION, None)
        if determination == None:
            return 0

        return determination.GetValue() * HEAL_INCREASE_PER_DETERMINATION

    def GetResourceComponentType(self):
        return repose.Repose
