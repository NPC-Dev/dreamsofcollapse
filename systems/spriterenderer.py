from esper import esper
from component import position, sprite
import pygame

class SpriteRenderer(esper.Processor):
    def __init__(self, display):
        esper.Processor.__init__(self)
        self.display = display
        self.displayRect = display.get_rect()

        self.loadedImages = {}

        self.cameraX = 0
        self.cameraY = 0

    def LoadOrGetImage(self, filename):
        image = self.loadedImages.get(filename)
        if image:
            return image

        image = pygame.image.load(filename)

        self.loadedImages[filename] = image
        return image

    def SetCameraPos(self, x, y):
        self.cameraX = x
        self.cameraY = y

    def Draw(self, sprite, x, y):
        tileImage = self.LoadOrGetImage(sprite.sheet)

        screenX = x - sprite.width/2 - self.cameraX
        screenY = y - sprite.height/2 - self.cameraY

        if (screenX+sprite.width/2 < 0 or \
            screenX-sprite.width/2 > self.displayRect.width or \
            screenY+sprite.height/2 < 0 or \
            screenY-sprite.height/2 > self.displayRect.height):
                return

        self.display.blit(tileImage, \
                            (screenX, screenY), \
                            area=(sprite.x, sprite.y, sprite.width, sprite.height))
    
    def process(self, deltaTime):
        for ent, (pos, img) in self.world.get_components(position.Position, sprite.Sprite):
            self.Draw(img, pos.x, pos.y) 
