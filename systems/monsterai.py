from esper import esper
from component import position, aistate, movementstate, rigidbody, enemytypes, commandqueue
from entities import entityutils
import pygame
import pymunk
from ai import pathsearch
from groundcollapse.tileset import Pos
import math
from command import movecommand
from command import attackcommand
import random
from pubsub import pub

VISION_RADIUS = 8

MAX_SCREENS_AWAY = 0.75

TIME_BETWEEN_PATH_UPDATES = 1.5
TIME_BETWEEN_REGULAR_UPDATES = 0.15

LINE_OF_SIGHT_APPROX_MULT = 1.05

MAX_TIME_PER_FRAME = 6

MOVE_RANDOM_PROBABILITY = 0.1

class MonsterAI(esper.Processor):
    def __init__(self, tileHolder, world, physicsWorld, tileSize):
        esper.Processor.__init__(self)
        self.tileSize = tileSize
        self.tileHolder = tileHolder
        self.positionTracker = tileHolder.GetPositionTracker()
        self.world = world
        self.physicsWorld = physicsWorld
        self.currTurn = 0
        pub.subscribe(self.OnTurnsPassed, "turns.passed")

    def OnTurnsPassed(self, newTurn, oldTurn):
        self.currTurn = newTurn

    def process(self, deltaTime):
        startTime = pygame.time.get_ticks()
        for ent, (moveState, pos, aiState, rigidBody, enemyTypes, commandQueue) in self.world.get_components(movementstate.MovementState, position.Position, aistate.AIState, rigidbody.RigidBody, enemytypes.EnemyTypes, commandqueue.CommandQueue):
            aiState.timeSinceUpdate += deltaTime
            aiState.timeSincePathUpdate += deltaTime
            
            if pygame.time.get_ticks() - startTime > MAX_TIME_PER_FRAME:
                continue

            if aiState.timeSinceUpdate > TIME_BETWEEN_REGULAR_UPDATES:
                aiState.timeSinceUpdate = 0
            else:
                continue
           
            # Check if we're too far from the camera to bother updating
            if not self.tileHolder.IsWithinScreensFromCamera(pos, MAX_SCREENS_AWAY):
                continue
                
            if aiState.blackboard.get("last_updated_initiative", 0) == self.currTurn:
                continue
            else:
                aiState.blackboard["last_updated_initiative"] = self.currTurn
            
            currMovePos = MonsterAI.GetEventualPosition(pos, moveState)
            currTilePos = self.WorldToTile(currMovePos) 

            adjacentTileRect = pygame.Rect(currTilePos.x-1, currTilePos.y-1, 3, 3)
            # Check if there is an enemy right next to us we should attack
            enemiesNextToUs = entityutils.QueryForEnemiesInTileRect(self.world, self.positionTracker, enemyTypes, adjacentTileRect)
            if enemiesNextToUs != None and len(enemiesNextToUs) > 0:
                # Remove any other commands we were waiting on.
                if len(commandQueue.commands) > 0:
                    commandQueue.commands.clear()

                enemy = enemiesNextToUs.pop()
                enemyPos = entityutils.GetEventualPosition(self.world, enemy)
                enemyTilePos = self.WorldToTile(enemyPos)

                enemyOffset = enemyTilePos - currTilePos
                
                commandQueue.commands.append(attackcommand.AttackCommand(self.tileHolder, self.world, self.physicsWorld, enemyOffset))
                continue

            currPath = None
            lastKnownEnemyLoc = aiState.blackboard.get("target_enemy_last_seen", None)
            if lastKnownEnemyLoc != None:
                enemies = self.CheckForEnemies(enemyTypes, lastKnownEnemyLoc)
                if enemies != None and len(enemies) > 0:
                    #print("I, entity " + str(ent) + " at pos: " + str(currMovePos.x) + ", " + str(currMovePos.y) + " see Enemy: " + str(enemies))
                    currPath = aiState.blackboard.get("path_to_enemy", None)
                    if currPath != None and len(currPath) > 0:
                        hasGoodPath = True
                        currStep = currPath[0]
                        offset = pygame.Vector2(currStep.x - currTilePos.x, 
                                                currStep.y - currTilePos.y)
                        
                        if abs(offset.x) > 1 or \
                            abs(offset.y) > 1:
                            # We are off our path and have to re-plan
                            hasGoodPath = False
                    else:
                        hasGoodPath = False

                    if not hasGoodPath:
                        aiState.blackboard["path_to_enemy"] = None
                        currPath = None
                else:
                    # Our enemy is no longer where they were,
                    # so our path may be invalid, but we can still try to follow it.
                    aiState.blackboard["target_enemy_in_view"] = False
                    aiState.timeSincePathUpdate += TIME_BETWEEN_PATH_UPDATES
                
                
            if currPath == None and aiState.timeSincePathUpdate >= TIME_BETWEEN_PATH_UPDATES:
                aiState.timeSincePathUpdate = 0
                currPath = self.TryToFindAnEnemy(rigidBody, enemyTypes, currMovePos, aiState, moveState, currPath)
            
            currPath = aiState.blackboard.get("path_to_enemy", None)
            if currPath != None and len(currPath) > 0:
                if len(commandQueue.commands) > 0:
                    continue

                #print("I, entity " + str(ent) + " at pos: " + str(math.floor(currMovePos.x/self.tileSize.x)) + ", " + str(math.floor(currMovePos.y/self.tileSize.y)) + " am following Path: " + str(currPath))
                currStep = currPath.pop(0)

                if len(currPath) == 0:
                    # TODO: Maybe just add a check earlier to see if we're already next to an enemy and can just attack, rather than this complicated way of re-pathing to get an attack command?
                    aiState.blackboard["path_to_enemy"] = None

                offset = pygame.Vector2(currStep.x - currTilePos.x, 
                                        currStep.y - currTilePos.y)
               
                if self.positionTracker.IsAnyEntityAtTile(currStep):
                    # We can't move to where another entity already is
                    continue 
                
                if abs(offset.x) > 1 or \
                    abs(offset.y) > 1:
                    # We are off our path and have to re-plan
                    aiState.blackboard["path_to_enemy"] = None
                else:
                    commandQueue.commands.append(movecommand.MoveCommand(self.tileHolder, self.world, self.physicsWorld, offset))

            if len(commandQueue.commands) == 0:
                # If we have nothing else to do, just do nothihng or move randomly.
                if random.random() < MOVE_RANDOM_PROBABILITY:
                    self.MoveRandomly(commandQueue)

    def MoveRandomly(self, commandQueue):
        offset = pygame.Vector2(random.randrange(-1,2), random.randrange(-1,2))
        commandQueue.commands.append(movecommand.MoveCommand(self.tileHolder, self.world, self.physicsWorld, offset))

    def WorldToTile(self, pos):
        return pygame.Vector2(math.floor(pos.x/self.tileSize.x), \
                                math.floor(pos.y/self.tileSize.y))

    def GetEventualPosition(positionComponent, moveState):
        pos = pygame.Vector2(positionComponent.x, positionComponent.y)

        if moveState.goalPos:
            pos = moveState.goalPos

        return pos

    def TryToFindAnEnemy(self, rigidBody, enemyTypes, positionComponent, aiState, moveState, currPath = None):
        pos = MonsterAI.GetEventualPosition(positionComponent, moveState)

        enemiesInRange = self.QueryForEnemies(rigidBody, enemyTypes, pos, VISION_RADIUS)
        for enemyQueryResult in enemiesInRange:
            enemyBody = enemyQueryResult.shape.body
            enemyPos = enemyBody.position
            pathSearch = pathsearch.PathSearch(self.tileHolder)
            pathStart = Pos(math.floor(pos.x/self.tileSize.x), 
                            math.floor(pos.y/self.tileSize.y))
            pathEnd = Pos(math.floor(enemyPos.x/self.tileSize.x),
                            math.floor(enemyPos.y/self.tileSize.y))
            path = pathSearch.Search(pathStart, pathEnd)
            if path != None and (currPath == None or len(path) < len(currPath) or
                                aiState.blackboard["target_enemy_in_view"] == None):
                if not self.HasApproximateLineOfSight(pos, enemyPos, path):
                    continue
                
                aiState.blackboard["target_enemy"] = enemyBody.entity
                aiState.blackboard["target_enemy_last_seen"] = pygame.Vector2(enemyPos.x, enemyPos.y)
                aiState.blackboard["target_enemy_in_view"] = True
                aiState.blackboard["path_to_enemy"] = path
                currPath = path 

        return currPath

    def HasApproximateLineOfSight(self, pos, enemyPos, path):
        expectedUnblockedDist = self.ComputeStraightLineMoveDist(pos, enemyPos)
        return len(path) < expectedUnblockedDist * LINE_OF_SIGHT_APPROX_MULT

    def ComputeStraightLineMoveDist(self, pos, enemyPos):
            pathStart = Pos(math.floor(pos.x/self.tileSize.x), 
                            math.floor(pos.y/self.tileSize.y))
            pathEnd = Pos(math.floor(enemyPos.x/self.tileSize.x),
                            math.floor(enemyPos.y/self.tileSize.y))

            # Chebyshev distance for movement on a grid
            return max(abs(pathStart.x - pathEnd.x), abs(pathStart.y - pathEnd.y))

    def CheckForEnemies(self, enemyTypes, pos):
        return entityutils.CheckForEnemies(self.world, self.positionTracker, enemyTypes, pymunk.Vec2d(pos.x, pos.y))
    
    def QueryForEnemies(self, rigidBody, enemyTypes, pos, radius):
        return entityutils.QueryForEnemiesUsingPhysics(rigidBody.body.space, enemyTypes, pymunk.Vec2d(pos.x, pos.y), radius * self.tileSize.x)
