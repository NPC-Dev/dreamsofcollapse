from systems import resourcerestorer
from component import clarity, stats

CLARITY_RESTORE_RATE = 0.0005

HEAL_INCREASE_PER_CLARITY = 0.0002

class ClarityRestorer(resourcerestorer.ResourceRestorer):
    def GetBaseRestore(self):
        return CLARITY_RESTORE_RATE

    def GetExtraComponents(self):
        return [stats.Stats]

    def GetBonuses(self, ent, extraComponents):
        statsComponent = extraComponents[0]
        creativity = statsComponent.stats.get(stats.StatType.CREATIVITY, None)
        if creativity == None:
            return 0

        return creativity.GetValue() * HEAL_INCREASE_PER_CLARITY

    def GetResourceComponentType(self):
        return clarity.Clarity
