from esper import esper
from component import position, rigidbody

class PhysicsSync(esper.Processor):
    def process(self, deltaTime):
        for ent, (pos, physics) in self.world.get_components(position.Position, rigidbody.RigidBody):
            physicspos = physics.body.position
            pos.x = physicspos.x - physics.offset.x
            pos.y = physicspos.y - physics.offset.y
