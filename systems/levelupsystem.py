from esper import esper
from component import refreshment, stats
from pubsub import pub

STAT_POINT_GAIN_PER_LEVEL = 1

VICTORY_LEVEL = 20

class LevelUpSystem(esper.Processor):
    def __init__(self):
        esper.Processor.__init__(self)

    def process(self, deltaTime):
        for ent, (refreshmentComp, statsComp) in self.world.get_components(refreshment.Refreshment, stats.Stats):
            if refreshmentComp.val < refreshmentComp.maxVal:
                continue
            
            currentLevel = statsComp.stats.get(stats.StatType.LEVEL, None)
            if currentLevel == None:
                continue
            
            currentLevel.AddValue(1)
            statsComp.stats[stats.StatType.LEVEL] = currentLevel
            
            pointsAvailable = statsComp.stats.get(stats.StatType.POINTSAVAILABLE, None)
            if pointsAvailable != None:
                pointsAvailable.AddValue(STAT_POINT_GAIN_PER_LEVEL)
                statsComp.stats[stats.StatType.POINTSAVAILABLE] = pointsAvailable

            if currentLevel.GetValue() == VICTORY_LEVEL:
                pub.sendMessage("menu.open", menu="victory")
            else:
                pub.sendMessage("entity.gainedlevel", entity=ent)

