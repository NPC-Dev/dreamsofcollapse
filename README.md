﻿# Dreams of Collapse

Journey through your own ever-shifting dreamscape as you seek to overcome your fears and anxieties. Wrest control of your dream from the creatures of your nightmares, lest you lose yourself to your dreams of collapse.

### Installing:
- git submodule update --init

- Install miniconda (https://docs.conda.io/en/latest/miniconda.html)

- conda update conda

- conda create -n dreamcollapse python=3.7

- conda activate dreamcollapse

- conda install -c potassco clingo=5.6.1 pandas=1.3.5

- conda install -c conda-forge pymunk=6.4.0 pypubsub=4.0.3

- python3 -m pip install -U pygame=2.3.2 pygame-gui=0.6.8
	(on some systems, this may be just "python install")

### Running:

You can run the following command to test the basic functionality:

python ./dreamsofcollapse.py

Add --help to see the many command line options that can change the program's behavior!


### Packaging:

 - conda install pyinstaller
 
 - pyinstaller dreamsofcollapse.spec

### Credits:

Art by Calciumtrice licensed under Creative Commons Attribution 3.0 license: https://creativecommons.org/licenses/by/3.0/
	https://opengameart.org/content/animated-wizard
	https://opengameart.org/content/outdoor-tileset (tileset was modified)

Art by Joe Williamson licensed under Creative Commons Attribution-ShareAlike 3.0 license: https://creativecommons.org/licenses/by-sa/3.0/ 
	https://opengameart.org/content/roguelike-monsters

Art by saint11 placed in public domain under Creative Commons 0 license: https://creativecommons.org/publicdomain/zero/1.0/ 
	https://opengameart.org/content/16x16-mage (modified)

Music by Nibana and Balancé licensed under the CC-BY-NC-SA 4.0 license: https://creativecommons.org/licenses/by-nc-sa/4.0/
	https://ektoplazm.com/free-music/nibana-shumatsu-no-tani	
	https://ektoplazm.com/free-music/nibana-earth-from-above	
	https://ektoplazm.com/free-music/bass-fiction

Music by GreatOwl licensed under the CC-BY 4.0 license: https://creativecommons.org/licenses/by/4.0/
	https://ektoplazm.com/free-music/greatowl-lilia-sin-mi

Megrim font by Daniel Johnson, licensed under the OFL: https://scripts.sil.org/OFL_web

Some code based on the following sources:

Nelson, M. J., & Smith, A. M. (2016). ASP with applications to mazes and levels. In Procedural Content Generation in Games (pp. 143–157). Springer.

Karth, I., & Smith, A. M. (2017). WaveFunctionCollapse is constraint solving in the wild. Proceedings of the 12th International Conference on the Foundations of Digital Games, 68. ACM.


