#! /usr/bin/python

import math
import pygame
import pygame_gui
from esper import esper
from entities import player
from systems import spriterenderer
from systems import physicssync
from systems import commandmanager
from systems import movementmanager
from systems import shakemanager
from systems import reposerestorer 
from systems import refreshmentrestorer 
from systems import clarityrestorer 
from systems import death
from systems import monsterai
from systems import computerepose
from systems import computeclarity
from systems import levelupsystem
from systems import computerefreshmentrequired
from component import position
from component import rigidbody
from component import sprite
from component import commandqueue
from component import movementstate
from component import stats
from component import refreshment
from command import movecommand
from physics import physicsworld
from procgen import objectdestroyer
from procgen import monsterspawner 
from procgen import tilephysicsspawner 
from ui import hud
from ui import gameover
from ui import mainmenu
from ui import areaselection
from ui import charactermenu
from ui import victory
from pubsub import pub
import inputmanager
import musicmanager
import positiontracker
import gamesettings
from entities import entityutils

black = (0,0,0)
ALPHA_KEY_COLOR=(255,255,0)

FPS = 60
DT_PHYSICS = 1.0/FPS

SQUARE_WIDTH = 16
SQUARE_HEIGHT = 16

MONSTER_KILL_EXPERIENCE_PER_LEVEL = 5
MONSTER_KILL_EXPERIENCE_PER_LEVEL_EXPONENT = 1.5

class LevelViewer:

    def __init__(self, 
                    store,
                    chunkSize,
                    display_width=1280, 
                    display_height=920, 
                    scale_x=2, 
                    scale_y=2,
                    maxCameraVel=1000.0,
                    cameraAccel=pygame.Vector2(500,500),
                    cameraDecelMod = 5.0,
                    debugPhysics=False,
                    tilesetData=None,
                    playerSpawnX=136,
                    playerSpawnY=136):
        pygame.init()
        self.quitting = False
        self.loadedImages = {}
        self.levelData = store
        self.chunkSize = chunkSize
        self.clock = pygame.time.Clock()
        self.display = pygame.display.set_mode((display_width, display_height))
        self.scale_x = scale_x
        self.scale_y = scale_y
        self.debugPhysics = debugPhysics
    
        self.mainMenu = None
        self.characterMenu = None
        self.victoryMenu = None

        self.paused = True
        self.gameInProgress = False
       
        self.gameSettings = None

        self.player = None
        self.playerEntity = None

        self.unscaledDisplay = pygame.Surface((int(display_width/scale_x), int(display_height/scale_y)))
        
        self.guiManager = pygame_gui.UIManager((display_width, display_height))
        self.LoadFonts()
        self.HUD = hud.HUD(pygame.Rect((0,0),(display_width,display_height)), self.guiManager)
        
        self.physicsDebugDisplay = pygame.Surface((int(display_width/scale_x), int(display_height/scale_y)))
        self.physicsDebugDisplay.set_colorkey(ALPHA_KEY_COLOR)
        self.physicsUpdateTime = 0.0
        
        self.unscaledDisplayRect = self.unscaledDisplay.get_rect()

        self.cameraVel = pygame.Vector2()
        self.desiredVel = pygame.Vector2()
       
        # TODO: load these from a config file?
        self.maxCameraVel = maxCameraVel
        self.cameraAccel = cameraAccel
        self.cameraDecelMod = cameraDecelMod

        self.cameraPos = pygame.Vector2()

        self.world = esper.World()

        self.physicsWorld = physicsworld.PhysicsWorld(self.physicsDebugDisplay)

        if tilesetData != None:
            self.SetTilesetData(tilesetData)

        self.musicManager = musicmanager.MusicManager()
        
        self.positionTracker = positiontracker.PositionTracker(self)

        self.InitSystems()
        
        self.inputManager = inputmanager.InputManager(self, self.world, self.physicsWorld, self.guiManager, tilesetData, self.chunkSize)
       
        self.areaSelection = areaselection.AreaSelection(self.GetTileSize(), self.chunkSize, pygame.Vector2(self.scale_x, self.scale_y), self.display)

        pub.subscribe(self.OnEntityDied, "died")

        pub.subscribe(self.OnTurnsPassed, "turns.passed")

        pub.subscribe(self.OnGameStart, "game.start")
        pub.subscribe(self.OnGameRestart, "game.restart")
        pub.subscribe(self.OnGamePaused, "game.paused")
        pub.subscribe(self.OnGameUnpaused, "game.unpaused")
        
        pub.subscribe(self.OnEntityLevelUp, "entity.gainedlevel")
        
        pub.subscribe(self.OnMenuOpenRequest, "menu.open")
        
        pub.subscribe(self.SaveSettings, "settings.save")
        pub.subscribe(self.RetrieveSettings, "settings.retrieve")
        
        self.RetrieveSettings()
        
        self.ShowMainMenu()
        
        self.currTurn = 0
        self.lastTick = 0

    def SaveSettings(self, settings):
        self.gameSettings = settings
        self.gameSettings.SaveSettings()

    def RetrieveSettings(self):
        if self.gameSettings == None:
            self.gameSettings = gamesettings.GameSettings()

        if self.gameSettings != None:
            pub.sendMessage("settings.loaded", settings=self.gameSettings)

    def LoadFonts(self):
        self.guiManager.add_font_paths(font_name='Megrim-Regular', regular_path='fonts/Megrim-Regular.ttf')
        self.guiManager.preload_fonts([{'name': 'Megrim-Regular', 'point_size': 48, 'style': 'regular'},
                                       {'name': 'Megrim-Regular', 'point_size': 24, 'style': 'regular'},
                                       {'name': 'Megrim-Regular', 'point_size': 14, 'style': 'regular'},
                                       ])

    def GetPositionTracker(self):
        return self.positionTracker

    def OnTurnsPassed(self, newTurn, oldTurn):
        self.currTurn = newTurn
    
    def SpawnPlayer(self, x, y, maxSearchRadius = 10):
        tileSize = self.GetTileSize()

        tileX = math.floor(x/tileSize.x)
        tileY = math.floor(y/tileSize.y)
        startTileX = tileX
        startTileY = tileY
        xOffset = 0
        yOffset = 0
        delta = pygame.Vector2(0, -1)

        foundValidSpawnPoint = False
        while not foundValidSpawnPoint:
            tile = self.GetTile(pygame.Vector2(tileX * tileSize.x, tileY * tileSize.y))
            if tile != None and tile.IsWalkable():
                foundValidSpawnPoint = True
                continue
            else:
                # Move in a spiral outward:
                if xOffset == yOffset or (xOffset < 0 and xOffset == -yOffset) \
                    or (xOffset > 0 and xOffset == 1 - yOffset):
                    delta = pygame.Vector2(-delta.y, delta.x)
                xOffset += delta.x
                yOffset += delta.y
                tileX = startTileX + xOffset
                tileY = startTileY + yOffset
                if pygame.Vector2(xOffset, yOffset).magnitude_squared() > maxSearchRadius * maxSearchRadius:
                    # We have gone too far. Stop searching.
                    return

                tile = self.GetTile(pygame.Vector2(tileX, tileY))

        playerPos = pygame.Vector2(tileX * tileSize.x + tileSize.x/2, 
                                    tileY * tileSize.y + tileSize.y/2)
        self.player = player.Player(self.world, self.physicsWorld, playerPos.x, playerPos.y, self.currTurn)
        self.playerEntity = self.player.GetEntity()

        pub.sendMessage("player.spawn", player=self.player, pos=playerPos)

    def InitSystems(self):
        self.world.add_processor(computerepose.ComputeRepose())
        self.world.add_processor(computeclarity.ComputeClarity())
        self.world.add_processor(computerefreshmentrequired.ComputeRefreshment())
        self.world.add_processor(levelupsystem.LevelUpSystem())

        self.world.add_processor(reposerestorer.ReposeRestorer())
        self.world.add_processor(clarityrestorer.ClarityRestorer())
        self.world.add_processor(refreshmentrestorer.RefreshmentRestorer())
        
        self.world.add_processor(monsterai.MonsterAI(self, self.world, self.physicsWorld, self.GetTileSize()))

        self.world.add_processor(commandmanager.CommandManager())
        
        self.world.add_processor(movementmanager.MovementManager(self.physicsWorld, self.GetTileSize()))
        
        self.world.add_processor(shakemanager.ShakeManager())

        self.world.add_processor(death.Death(self.physicsWorld))

        physicsSync = physicssync.PhysicsSync()
        self.world.add_processor(physicsSync)

        self.spriteRenderer = spriterenderer.SpriteRenderer(self.unscaledDisplay)
        self.world.add_processor(self.spriteRenderer)

    def OnEntityLevelUp(self, entity):
        if entity == self.playerEntity:
            self.ShowCharacterMenu()    

    def OnEntityDied(self, entity, pos, dueToDamage=False):
        if entity == self.playerEntity:
            self.playerEntity = None
        
            self.gameInProgress = False
            gameover.GameOver(pygame.Rect((10,10), (self.display.get_rect().width-10, self.display.get_rect().height-10)), self.guiManager)
        else:
            if not dueToDamage:
                # The entity died due to some other cause
                return

            if self.playerEntity == None:
                return

            monsterLevel = 1
            enemyStatsComponent = self.world.try_component(entity, stats.Stats)
            if enemyStatsComponent:
                levelStat = enemyStatsComponent.stats.get(stats.StatType.LEVEL, None)
                if levelStat != None:
                    monsterLevel = levelStat.GetValue()

            refreshmentComponent = self.world.try_component(self.playerEntity, refreshment.Refreshment)
            if refreshmentComponent:
                # Add experience!
                refreshmentBonus = MONSTER_KILL_EXPERIENCE_PER_LEVEL * math.pow(monsterLevel, MONSTER_KILL_EXPERIENCE_PER_LEVEL_EXPONENT)

                refreshmentComponent.val += refreshmentBonus
                
    def OnMenuOpenRequest(self, menu):
        if menu == "main":
            self.ShowMainMenu()
        elif menu == "character":
            self.ShowCharacterMenu()
        elif menu == "victory":
            self.ShowVictoryMenu()

    def ShowVictoryMenu(self):
        if self.victoryMenu == None:
            pub.sendMessage("game.paused")
            self.paused = True
            self.victoryMenu = victory.Victory(self.display.get_rect(), self.guiManager)

    def ShowMainMenu(self):
        if self.mainMenu == None:
            pub.sendMessage("game.paused")
            self.paused = True
            self.mainMenu = mainmenu.MainMenu(self.display.get_rect(), self.guiManager, gameInProgress = self.gameInProgress)

    def ShowCharacterMenu(self):
        if self.characterMenu == None:
            pub.sendMessage("game.paused")
            self.paused = True
            rect = self.display.get_rect()
            rect.x += 100
            rect.y += 100
            rect.width -= 200
            rect.height -= 200
            self.characterMenu = charactermenu.CharacterMenu(rect, self.guiManager, self.playerEntity, self.world)

    def OnGamePaused(self):
        self.paused = True

    def OnGameUnpaused(self):
        self.characterMenu = None
        self.mainMenu = None
        self.victoryMenu = None
        self.paused = False

    def OnGameStart(self):
        if not self.gameInProgress:
            self.gameInProgress = True
            self.SpawnPlayer(self.cameraPos.x + self.unscaledDisplayRect.width/2, self.cameraPos.y + self.unscaledDisplayRect.height/2)
        pub.sendMessage("game.unpaused")
        self.paused = False

    def OnGameRestart(self):
        self.gameInProgress = True
        pub.sendMessage("game.unpaused")
        self.paused = False
        self.levelData.Clear()
        self.SpawnPlayer(self.cameraPos.x, self.cameraPos.y)

    def SetTilesetData(self, tileset):
        self.tileset = tileset
       
        self.objectDestroyer = objectdestroyer.ObjectDestroyer(self.tileset, self.world, self.physicsWorld)
        self.monsterSpawner = monsterspawner.MonsterSpawner(self.tileset, self.world, self.physicsWorld, self.playerEntity)
        #self.tilePhysicsSpawner = tilephysicsspawner.TilePhysicsSpawner(self.tileset, self.world, self.physicsWorld)

    def RemoveTiles(self, x, y):
        self.levelData.RemoveTiles(x, y)

    def SetLevelStore(self, store):
        self.levelData = store


    def Tick(self):
        if self.quitting:
            return

        newTime = pygame.time.get_ticks()
        tickDiff = (newTime - self.lastTick)/1000.0
        self.lastTick = newTime

        if not self.inputManager.ProcessInput(tickDiff):
            self.quitting = True
            return

        if not self.paused:
            self.physicsUpdateTime += tickDiff

            self.ProcessCameraMovement(tickDiff)

            if self.playerEntity == None and self.gameInProgress:
                self.SpawnPlayer(self.cameraPos.x, self.cameraPos.y)

        self.Clear()

        self.DrawLevel()

        self.spriteRenderer.SetCameraPos(self.cameraPos.x, self.cameraPos.y)

        if not self.paused:
            self.world.process(tickDiff)

        self.guiManager.update(tickDiff)
      
        '''
        spawnerStart = pygame.time.get_ticks()
        self.tilePhysicsSpawner.Tick(spawnerStart, tickDiff)
        spawnerTime = pygame.time.get_ticks() - spawnerStart
        if spawnerTime > 10:
            print("Tile Physics spawner time: " + str(spawnerTime))
        '''
        if not self.paused:
            spawnerStart = pygame.time.get_ticks()
            self.monsterSpawner.Tick(spawnerStart, tickDiff)

        self.display.blit(pygame.transform.scale(self.unscaledDisplay, self.display.get_rect().size), (0, 0))
        
        self.areaSelection.Draw(self.cameraPos, tickDiff)

        if self.playerEntity != None:
            self.HUD.Update(self.world, self.playerEntity)

        self.guiManager.draw_ui(self.display)

        if self.debugPhysics:
            self.physicsDebugDisplay.fill(ALPHA_KEY_COLOR)
            self.physicsWorld.DebugDraw()
            
            self.display.blit(pygame.transform.scale(self.physicsDebugDisplay, self.display.get_rect().size), (-self.cameraPos.x*self.scale_x, -self.cameraPos.y*self.scale_y))

        if not self.paused:
            while (self.physicsUpdateTime > DT_PHYSICS):
                self.physicsWorld.GetSpace().step(DT_PHYSICS)
                
                # In theory, we should probably be updating physics for multiple time steps if
                # it has been too long since the last update, but it turns out that we get a more
                # stable and better looking simulation if we only run one timestep per frame.
                # So... just pretend we are up to date.
                self.physicsUpdateTime = 0 #-= DT_PHYSICS
        
        pygame.display.update()

        frameTimeTotal = pygame.time.get_ticks() - self.lastTick
        #if frameTimeTotal > (1000/FPS)*2:
            #print("Frame time: " + str(frameTimeTotal))

        self.clock.tick(FPS)

        return True

    def Clear(self):
        self.display.fill((black))
        self.unscaledDisplay.fill((black))

    def DrawLevel(self):
        minChunk = self.ToTileCoords(self.cameraPos)
        minChunk.x /= self.chunkSize.x
        minChunk.y /= self.chunkSize.y
        maxChunk = self.ToTileCoords(self.cameraPos + self.unscaledDisplayRect.size)
        maxChunk.x /= self.chunkSize.x
        maxChunk.y /= self.chunkSize.y
        maxChunk.x += 1
        maxChunk.y += 1
        
        for chunkX in range(math.floor(minChunk.x), math.floor(maxChunk.x)):
            for chunkY in range(math.floor(minChunk.y), math.floor(maxChunk.y)):
                tiles = self.levelData.GetTiles(chunkX * self.chunkSize.x, chunkY * self.chunkSize.y)

                self.DrawChunk(pygame.Vector2(chunkX, chunkY), tiles)

    def GetTile(self, pos):
        tileCoordExact = self.ToTileCoords(pos)
        tileCoord = pygame.Vector2(math.floor(tileCoordExact.x), math.floor(tileCoordExact.y))
        chunkCoord = pygame.Vector2(math.floor(tileCoord.x / self.chunkSize.x) * self.chunkSize.x, math.floor(tileCoord.y / self.chunkSize.y) * self.chunkSize.y)

        tilePos = tileCoord - chunkCoord

        tiles = self.levelData.GetTiles(int(chunkCoord.x), int(chunkCoord.y))

        if tiles == None or len(tiles) == 0:
            return None

        tileType = tiles[int(tilePos.x)][int(tilePos.y)]
        tileMetadata = self.tileset.GetTile(tileType)
        if tileMetadata == None:
            return None
        return tileMetadata
    
    def GetTileFromGridCoords(self, tileCoord):
        chunkCoord = pygame.Vector2(math.floor(tileCoord.x / self.chunkSize.x) * self.chunkSize.x, math.floor(tileCoord.y / self.chunkSize.y) * self.chunkSize.y)

        tilePos = tileCoord - chunkCoord

        tiles = self.levelData.GetTiles(int(chunkCoord.x), int(chunkCoord.y))

        if tiles == None or len(tiles) == 0:
            return None

        tileType = tiles[int(tilePos.x)][int(tilePos.y)]
        tileMetadata = self.tileset.GetTile(tileType)
        if tileMetadata == None:
            return None
        return tileMetadata

    def DrawChunk(self, chunkPos, tiles):
        tileSize = self.GetTileSize()
        
        for x in range(0, len(tiles)):
            for y in range(0, len(tiles[0])):
                tileScreenX = (chunkPos.x * self.chunkSize.x + x) * tileSize.x - self.cameraPos.x
                tileScreenY = (chunkPos.y * self.chunkSize.y + y) * tileSize.y - self.cameraPos.y
                tileRect = pygame.Rect(tileScreenX, tileScreenY, tileSize.x, tileSize.y)
                if not tileRect.colliderect(self.unscaledDisplayRect):
                    continue
                self.DrawTile(tiles[x][y], tileScreenX, tileScreenY)

    def IsWithinScreensFromCamera(self, pos, MAX_SCREENS_AWAY):
        cameraRelativeX = pos.x - (self.cameraPos.x + self.unscaledDisplayRect.width/2)
        cameraRelativeY = pos.y - (self.cameraPos.y + self.unscaledDisplayRect.height/2)
        screenX = cameraRelativeX/self.unscaledDisplayRect.width
        screenY = cameraRelativeY/self.unscaledDisplayRect.height
        return pygame.Vector2(screenX, screenY).magnitude_squared() <= MAX_SCREENS_AWAY * MAX_SCREENS_AWAY

    def ToTileCoords(self, pos):
        tilesize = self.GetTileSize()
        return pygame.Vector2(pos.x / tilesize.x, pos.y / tilesize.y)

    def GetTileSize(self):
        return self.tileset.GetTileSize()

    def CenterCoordsOnTile(self, pos):
        tileSize = self.GetTileSize()
        return pygame.Vector2(math.floor(pos.x/tileSize.x) * tileSize.x + tileSize.x/2,
                                    math.floor(pos.y/tileSize.y) * tileSize.y + tileSize.y/2)

    def DrawTile(self, tile, x, y):
        tileMetadata = self.tileset.GetTile(tile)
        if tileMetadata == None:
            return

        pos = tileMetadata.GetPos()
        size = self.GetTileSize()

        tileImage = self.LoadOrGetImage(tileMetadata.GetFile())

        self.unscaledDisplay.blit(tileImage, (x,y), (pos.x, pos.y, size.x, size.y))
        
        if self.debugPhysics:
            pygame.draw.rect(self.unscaledDisplay, (80,80,80), (x, y, size.x, size.y), 1)

    def GetPlayerPos(self):
        playerPos = self.world.component_for_entity(self.playerEntity, position.Position)
        return pygame.Vector2(playerPos.x, playerPos.y)

    def GetPlayerTilePos(self):
        playerPos = self.GetPlayerPos() 
        tileSize = self.GetTileSize()
        return pygame.Vector2(math.floor(playerPos.x/tileSize.x), math.floor(playerPos.y/tileSize.y))

    def ProcessCameraMovement(self, timeDiff):
        if self.playerEntity == None:
            return
        
        self.desiredVel = self.cameraVel * self.cameraDecelMod * -1.0
        
        self.cameraDeadZoneWidth = 32
        self.cameraDeadZoneHeight = 32
        cameraCenterX = self.cameraPos.x + self.unscaledDisplayRect.width/2
        cameraCenterY = self.cameraPos.y + self.unscaledDisplayRect.height/2
        
        playerPos = self.GetPlayerPos()

        cameraAccelScaleX = min(1, abs((cameraCenterX-playerPos.x)/(self.unscaledDisplayRect.width/2)))
        cameraAccelScaleY = min(1, abs((cameraCenterY-playerPos.y)/(self.unscaledDisplayRect.height/2)))

        if cameraCenterX < playerPos.x - self.cameraDeadZoneWidth:
            self.desiredVel.x += self.cameraAccel.x * cameraAccelScaleX
        
        if cameraCenterX > playerPos.x + self.cameraDeadZoneWidth:
            self.desiredVel.x -= self.cameraAccel.x * cameraAccelScaleX

        if cameraCenterY < playerPos.y - self.cameraDeadZoneHeight:
            self.desiredVel.y += self.cameraAccel.y * cameraAccelScaleY

        if cameraCenterY > playerPos.y + self.cameraDeadZoneHeight:
            self.desiredVel.y -= self.cameraAccel.y * cameraAccelScaleY

        self.cameraVel = self.cameraVel.lerp(self.desiredVel, min(timeDiff,1.0))
        if self.cameraVel.magnitude_squared() > (self.maxCameraVel * self.maxCameraVel):
            self.cameraVel.scale_to_length(self.maxCameraVel)

        self.cameraPos += self.cameraVel * timeDiff

    def SetCamera(self, x, y):
        self.cameraPos.x = x
        self.cameraPos.y = y
    
    def SetCamera(self, vector):
        self.cameraPos = vector

    def GetCameraRect(self):
        result = pygame.Rect(self.ToTileCoords(self.cameraPos), 
                            self.ToTileCoords(pygame.Vector2(self.unscaledDisplayRect.size)))
        return result

    def GetCameraCenter(self):
        result = self.ToTileCoords(pygame.Vector2(self.cameraPos.x + self.unscaledDisplayRect.width/2, 
                            self.cameraPos.y + self.unscaledDisplayRect.height/2))
        return result

    def LoadOrGetImage(self, filename):
        image = self.loadedImages.get(filename)
        if image:
            return image

        image = pygame.image.load(filename)
        image = image.convert()

        self.loadedImages[filename] = image
        return image

if __name__ == "__main__":
    viewer = LevelViewer()
    while(viewer.Tick()):
        continue

    quit()
