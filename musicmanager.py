import sys
import pygame
from pubsub import pub
import random

VOLUME = 100.0

class MusicManager:
    SONG_END_EVENT = pygame.USEREVENT + 1

    def __init__(self):
        pub.subscribe(self.SongEnded, "song.ended")
        self.InitSongList()

        self.mainVolume = 100.0
        self.musicVolume = VOLUME
        self.UpdateMusicVolume()

        pub.subscribe(self.OnSettingUpdated, "setting.updated")

        self.SongEnded()

    def OnSettingUpdated(self, settings, key):
        if key == "mainvolume":
            self.SetMainVolume(settings.GetValue("mainvolume"))
        elif key == "musicvolume":
            self.SetMusicVolume(settings.GetValue("musicvolume"))

    def SetMainVolume(self, mainVolume):
        self.mainVolume = max(0, min(100, mainVolume))
        self.UpdateMusicVolume()
    
    def SetMusicVolume(self, musicVolume):
        self.musicVolume = max(0, min(100, musicVolume))
        self.UpdateMusicVolume()
    
    def UpdateMusicVolume(self):
        pygame.mixer.music.set_volume(self.musicVolume/100.0 * self.mainVolume/100.0)

    def InitSongList(self):
        self.songs = {
                        "sound/music/Nibana/Orchestra-Electronica.ogg": 1,
                        "sound/music/Nibana/Moonset.ogg": 1,
                        "sound/music/Nibana/ADiveIntoEther.ogg": 1,
                        "sound/music/Nibana/GodsFromTheMachine.ogg": 1,
                        "sound/music/Nibana/EarthFromAbove.ogg": 1,
                        "sound/music/GreatOwl/Einimali.ogg": 1,
                        "sound/music/GreatOwl/HiuSi.ogg": 1,
                        "sound/music/GreatOwl/KiuSeiKai.ogg": 1,
                        "sound/music/GreatOwl/Rakate.ogg": 1,
                        "sound/music/GreatOwl/Strijistela.ogg": 1,
                        "sound/music/Balance/ItsJustWater.ogg": 1,
                        }

    def PlaySong(self, song):
        pygame.mixer.music.set_endevent(MusicManager.SONG_END_EVENT)
        print("Song: " + song)
        pygame.mixer.music.load(song)
        pygame.mixer.music.play()

    def GetRandomSongWithLowestPlayCount(self):
        lowestPlayCount = sys.maxsize
        for (key, value) in self.songs.items():
            lowestPlayCount = min(lowestPlayCount, value)
        listOfKeys = [key for (key, value) in self.songs.items() if value == lowestPlayCount]

        return random.choice(listOfKeys)

    def SongEnded(self):
        song = self.GetRandomSongWithLowestPlayCount()
        self.songs[song] += 1
        self.PlaySong(song)
