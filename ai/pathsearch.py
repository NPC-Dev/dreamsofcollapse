import queue
from dataclasses import dataclass, field
from typing import Any
from entities import entityutils
from groundcollapse.tileset import Pos
import pygame
import sys

@dataclass(order=True)
class TileData:
    priority: int
    tile: Any=field(compare=False)

class PathSearch:
    def __init__(self, tileHolder, heuristicMultiplier = 1.0):
        self.tileHolder = tileHolder
        self.heuristicMultiplier = heuristicMultiplier

    def Search(self, start, end, maxSearchSize=1000):
        self.start = start
        self.end = end

        if start == end:
            return []

        self.openList = queue.PriorityQueue()
        self.openList.put(TileData(priority=0, tile=self.start))

        self.closedList = set([])

        self.parents = {}
        self.parents[self.start] = None

        self.costSoFar = {}
        self.costSoFar[self.start] = 0

        self.searchedTiles = 0
        while not self.openList.empty():
            current = self.openList.get()
            
            if current.tile.x == self.end.x and \
                current.tile.y == self.end.y:
                return self.Reconstruct(self.end, self.parents)

            if current.tile in self.closedList:
                continue
            else:
                self.closedList.add(current.tile)
            self.searchedTiles += 1
            if self.searchedTiles > maxSearchSize:
                return None

            for neighbor in self.GetNeighbors(current.tile, self.parents[current.tile]):
                newCost = self.costSoFar[current.tile] + self.GetCost(current.tile, neighbor)

                prevCost = self.costSoFar.get(neighbor, sys.maxsize)
                if newCost < prevCost:
                    self.costSoFar[neighbor] = newCost
                    priority = newCost + self.GetHeuristic(neighbor, self.end)
                    self.openList.put(TileData(priority=priority, tile=neighbor))
                    self.parents[neighbor] = current

        return None

    def GetNeighbors(self, node, parent):
        neighbors = []
        for x in range(-1, 2): 
            for y in range(-1, 2):
                neighbor = Pos(node.x + x, node.y + y)
                if neighbor == parent:
                    continue
                if entityutils.CanMoveToGridCoords(self.tileHolder, pygame.Vector2(node.x, node.y), pygame.Vector2(neighbor.x, neighbor.y)):
                    neighbors.append(neighbor)

        return neighbors

    def GetCost(self, current, neighbor):
        # Chebyshev distance
        return max(abs(neighbor.x - current.x), abs(neighbor.y - current.y))
    
    def GetHeuristic(self, node, end):
        return self.GetCost(node, end) * self.heuristicMultiplier

    def Reconstruct(self, end, parents):
        result = [end]
        parent = parents[end]
        while parents[parent.tile] != None:
            result.insert(0, parent.tile)
            parent = parents[parent.tile]

        return result
