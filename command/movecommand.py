from component import movementstate, shake, enemytypes
from command import command, attackcommand
import pygame
import pymunk
from entities import entityutils
import math
from pubsub import pub
import positiontracker

SHAKE_TIME = 0.05
SHAKE_STRENGTH = 5000

class MoveCommand(command.Command):
    def __init__(self, tileHolder, world, physicsWorld, offset):
        command.Command.__init__(self)
        self.offset = offset
        self.world = world
        self.physicsWorld = physicsWorld
        self.tileHolder = tileHolder
        self.positionTracker = tileHolder.GetPositionTracker()
        self.tileSize = tileHolder.GetTileSize()

    def execute(self, ent):
        if self.offset.magnitude_squared() == 0:
            # TODO: Make a "do nothing command"
            return True

        if abs(self.offset.x) > 1 or abs(self.offset.y) > 1:
            print("Error: invalid offset for move command: " + str(self.offset))
            return False

        pos = entityutils.GetEventualPosition(self.world, ent)
        moveState = self.world.try_component(ent, movementstate.MovementState)
        if moveState:
            newPos = pos + pygame.Vector2(self.offset.x * self.tileSize.x, self.offset.y * self.tileSize.y)
            newPos = self.tileHolder.CenterCoordsOnTile(newPos) 
           
            # Check for permanent obstacles
            if not self.CanEverMoveTo(ent, pos, newPos):
                moveState.goalPos = pos
                if self.offset.magnitude_squared() > 0:
                    self.world.add_component(ent, shake.Shake(SHAKE_TIME, SHAKE_STRENGTH, self.offset.normalize()))
                return False

            # Check if an enemy is there and we should attack it
            enemyComponent = self.world.try_component(ent, enemytypes.EnemyTypes)
            if enemyComponent:
                enemies = entityutils.CheckForEnemies(self.world, self.positionTracker, enemyComponent, newPos)
                if enemies != None and len(enemies) > 0:
                    attackCommand = attackcommand.AttackCommand(self.tileHolder, self.world, self.physicsWorld, self.offset)
                    attackCommand.execute(ent)
                    return False
            
            # Check for temporary obstacles that would prevent movement
            if self.positionTracker.IsAnyEntityAt(newPos):
                if self.offset.magnitude_squared() > 0:
                    self.world.add_component(ent, shake.Shake(SHAKE_TIME, SHAKE_STRENGTH, self.offset.normalize()))
                return False 

            moveState.lastGoodPos = moveState.goalPos
            moveState.goalPos = newPos
            pub.sendMessage("entity.move", entity=ent, oldPos=moveState.lastGoodPos, newPos=moveState.goalPos)

    def CanEverMoveTo(self, entity, pos, newPos):
        if not entityutils.CanMoveTo(self.tileHolder, pos, newPos):
            return False
        return True
        
