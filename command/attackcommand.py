from command import command
from entities import entityutils
from component import enemytypes, shake, repose, commandqueue, stats
import pygame
import math

SHAKE_TIME = 0.1
SHAKE_STRENGTH = 10000

DAMAGE_SCALING_EXPONENT = 1.5

class AttackCommand(command.Command):
    def __init__(self, tileHolder, world, physicsWorld, offset):
        command.Command.__init__(self)
        self.offset = offset
        self.world = world
        self.physicsWorld = physicsWorld
        self.tileHolder = tileHolder
        self.positionTracker = tileHolder.GetPositionTracker()
        self.tileSize = tileHolder.GetTileSize()

    def execute(self, ent):
        pos = entityutils.GetEventualPosition(self.world, ent)
        newPos = pos + pygame.Vector2(self.offset.x * self.tileSize.x, self.offset.y * self.tileSize.y)
        
        enemyComponent = self.world.try_component(ent, enemytypes.EnemyTypes)
        if enemyComponent:
            enemies = entityutils.CheckForEnemies(self.world, self.positionTracker, enemyComponent, newPos)
            if enemies == None or len(enemies) == 0:
                return False
       
        # Attack the first enemy in the square, if more than one
        enemy = enemies.pop()

        if not entityutils.CanMoveTo(self.tileHolder, pos, newPos):
            return False

        if (self.offset.magnitude_squared() != 0):
            self.world.add_component(ent, shake.Shake(SHAKE_TIME, SHAKE_STRENGTH, self.offset.normalize()))
            self.world.add_component(enemy, shake.Shake(SHAKE_TIME, SHAKE_STRENGTH, vector=None))#, self.offset.normalize()))
        
        reposeComponent = self.world.try_component(enemy, repose.Repose)
        if reposeComponent:
            reposeComponent.val -= self.CalculateDamage(ent, enemy)

    def CalculateDamage(self, ent, enemy):
        damage = 0
        statsComponent = self.world.try_component(ent, stats.Stats)
        if statsComponent:
            willpower = statsComponent.stats.get(stats.StatType.WILLPOWER, None)
            if willpower == None:
                damage = 1
            else:
                damage += int(math.pow(willpower.GetValue(), DAMAGE_SCALING_EXPONENT))
        
        return damage
