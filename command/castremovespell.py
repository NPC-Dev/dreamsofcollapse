from command import command
from component import clarity

CLARITY_COST_PER_CHUNK = 1

class CastRemoveSpell(command.Command):
    def __init__(self, world, entity, areaSelected, tileHolder, chunkSize, tileSize):
        command.Command.__init__(self)
        self.areaSelected = areaSelected
        self.chunkSize = chunkSize
        self.tileSize = tileSize
        self.tileHolder = tileHolder
        self.world = world
        self.entity = entity

    def execute(self, ent):
        if not self.world.has_component(self.entity, clarity.Clarity):
            print("Error: no clarity component for our entity: " + str(self.entity))
            return

        clarityComponent = self.world.component_for_entity(self.entity, clarity.Clarity)

        for x in range(self.areaSelected.x, self.areaSelected.x + self.areaSelected.width):
            for y in range(self.areaSelected.y, self.areaSelected.y + self.areaSelected.height):
                if clarityComponent.val < CLARITY_COST_PER_CHUNK:
                    # We don't have enough clarity.
                    # TODO: show a notification and/or play a sound?
                    print("Not enough clarity.")
                    return

                # Convert chunk number to world pos
                realX = x * self.chunkSize.x
                realY = y * self.chunkSize.y

                self.tileHolder.RemoveTiles(realX, realY)

                # Spend clarity:
                clarityComponent.val -= CLARITY_COST_PER_CHUNK

