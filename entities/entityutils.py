from component import position, movementstate, clarity, enemytypes
import pygame
import pymunk
from physics.physicsworld import Masks
from groundcollapse.tileset import Dir
from groundcollapse.tileset import Pos

CLARITY_PER_CHUNK = 1

def GetEventualPosition(world, ent):
    positionComponent = world.component_for_entity(ent, position.Position)
    
    pos = pygame.Vector2(positionComponent.x, positionComponent.y)

    moveState = world.try_component(ent, movementstate.MovementState)
    if moveState:
        if moveState.goalPos:
            pos = moveState.goalPos

    return pos
    
def CheckForEnemyUsingPhysics(space, enemyComponent, newPos, radius=6):
    queryResults = QueryForEnemiesUsingPhysics(space, enemyComponent, newPos, radius)
    for result in queryResults:
        return result.shape.body.entity

def QueryForEnemiesUsingPhysics(space, enemyComponent, newPos, radius=6):
    enemyShapeFilter = GetEnemyFilter(enemyComponent)

    queryResults = space.point_query(newPos, radius, enemyShapeFilter)
    return queryResults

def GetEnemyFilter(enemyComponent):
    combinedCategory = 0b0
    for category in enemyComponent.enemyCategories:
        shapeFilter = Masks.GetCategory(category)
        combinedCategory = combinedCategory | shapeFilter.categories
        
    enemyShapeFilter = pymunk.ShapeFilter(mask=combinedCategory)
    return enemyShapeFilter

def CheckForEnemies(world, positionTracker, enemyComponent, pos):
    enemiesToReturn = set([])
    entitiesAtPos = positionTracker.GetEntitySet(pos)
    for entity in entitiesAtPos:
        theirEnemyComponent = world.try_component(entity, enemytypes.EnemyTypes)
        if theirEnemyComponent:
            if not enemyComponent.enemyCategories.isdisjoint(theirEnemyComponent.myCategories):
                enemiesToReturn.add(entity)

    return enemiesToReturn

def CheckForEnemiesAtTile(world, positionTracker, enemyComponent, tilePos):
    enemiesToReturn = set([])
    entitiesAtPos = positionTracker.GetEntitySetAtTile(tilePos)
    for entity in entitiesAtPos:
        theirEnemyComponent = world.try_component(entity, enemytypes.EnemyTypes)
        if theirEnemyComponent:
            if not enemyComponent.enemyCategories.isdisjoint(theirEnemyComponent.myCategories):
                enemiesToReturn.add(entity)

    return enemiesToReturn

def QueryForEnemiesInTileRect(world, positionTracker, enemyComponent, rect):
    enemiesToReturn = set([])
    for x in range(rect.x, rect.x + rect.width):
        for y in range(rect.y, rect.y + rect.height):
            enemiesInThisSquare = CheckForEnemiesAtTile(world, positionTracker, enemyComponent, Pos(x,y))
            enemiesToReturn.update(enemiesInThisSquare)

    return enemiesToReturn

def QueryForDynamicPathBlockers(space, newPos, radius=6):
    pathBlockerFilter = GetDynamicPathBlockerFilter()
    queryResults = space.point_query(newPos, radius, pathBlockerFilter)
    return queryResults

def GetDynamicPathBlockerFilter():
    return Masks.GetDynamicPathBlockerFilter()

def HasClarityRequiredForArea(entity, world, areaSize):
    clarityComponent = world.try_component(entity, clarity.Clarity)
    if clarityComponent:
        return clarityComponent.val >= areaSize.x * areaSize.y * CLARITY_PER_CHUNK
    return False

def CanMoveTo(tileHolder, pos, newPos):
    currTile = tileHolder.GetTile(pos)
    newTile = tileHolder.GetTile(newPos)

    if newTile == None or not newTile.IsWalkable():
        return False

    if currTile == None:
        return False

    posOffset = newPos - pos
    if not currTile.CanExit(Dir.FromPosOffset(posOffset.x, posOffset.y)):
        return False

    return True

def CanMoveToGridCoords(tileHolder, pos, newPos):
    currTile = tileHolder.GetTileFromGridCoords(pos)
    newTile = tileHolder.GetTileFromGridCoords(newPos)

    if newTile == None or not newTile.IsWalkable():
        return False

    if currTile == None:
        return False

    posOffset = newPos - pos
    if not currTile.CanExit(Dir.FromPosOffset(posOffset.x, posOffset.y)):
        return False

    return True
