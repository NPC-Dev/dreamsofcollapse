import pygame
import pymunk
from esper import esper
from component import position, rigidbody
from physics import physicsworld

DENSITY = 100

BEVEL_RADIUS = 1
CORNER_RADIUS = 2

WALL_PERCENT=0.5

EDGE_OFFSET_MULT = 0.70

class TileEntity():
    def __init__(self, world, tileSize, physicsWorld, tileMetadata, xPos, yPos):
        self.entity = world.create_entity(
                                #position.Position(x=xPos, y=yPos),
                            )

        
        body = rigidbody.EntityBody(self.entity, body_type = pymunk.Body.STATIC)
        body.position = (xPos + tileSize.x/2, yPos + tileSize.y/2)
        physicsToAdd = [body]

        halfWidth = tileSize.x/2
        halfHeight = tileSize.y/2

        exitDirData = tileMetadata.GetExitDirData()
        for moveDir in exitDirData:
            collider = None
            canMove = exitDirData[moveDir]
            if canMove:
                continue
           
            if moveDir.x == 0 or moveDir.y == 0:
                # This is a side wall
                if moveDir.x != 0:
                    offset_start = pymunk.Vec2d(moveDir.x * halfWidth * EDGE_OFFSET_MULT, -WALL_PERCENT * halfHeight) 
                    offset_end = pymunk.Vec2d(moveDir.x * halfWidth * EDGE_OFFSET_MULT, WALL_PERCENT * halfHeight) 
                elif moveDir.y != 0:
                    offset_start = pymunk.Vec2d(-WALL_PERCENT * halfWidth, moveDir.y * halfHeight * EDGE_OFFSET_MULT) 
                    offset_end = pymunk.Vec2d(WALL_PERCENT * halfWidth, moveDir.y * halfHeight * EDGE_OFFSET_MULT) 

                collider = pymunk.Segment(body, offset_start, offset_end, BEVEL_RADIUS)

            else:
                # This is a corner
                collider = pymunk.Circle(body, CORNER_RADIUS, offset=(moveDir.x * halfWidth * EDGE_OFFSET_MULT, 
                                                                        moveDir.y * halfHeight * EDGE_OFFSET_MULT))

            collider.density = DENSITY
            collider.filter = pymunk.ShapeFilter(
                                        categories = physicsworld.Masks.GetCategory("tile").categories, 
                                        mask = physicsworld.Masks.GetCategory("player").categories | 
                                            physicsworld.Masks.GetCategory("monster").categories) 
            physicsToAdd.append(collider)

        physicsWorld.GetSpace().add(*physicsToAdd)
        
        rigidComponent = rigidbody.RigidBody(body, pygame.Vector2(0, 0))
        world.add_component(self.entity, rigidComponent)
        
    def GetEntity(self):
        return self.entity
