from entities import monster
from component import stats
from component import sprite

WIDTH=16
HEIGHT=16

class Spider(monster.Monster):
    def GetSprite(self):
        return sprite.Sprite(sheet="sprites/joecreates/roguelikecreatures.png", x=16, y=64, width=WIDTH, height=HEIGHT)

    def GetStats(self):
        return stats.Stats(statsDict =
                    {
                        stats.StatType.DETERMINATION: stats.Stat(value=1),
                        stats.StatType.CREATIVITY: stats.Stat(value=0),
                        stats.StatType.WILLPOWER: stats.Stat(value=1),
                        stats.StatType.WIT: stats.Stat(value=2),
                        stats.StatType.LEVEL: stats.Stat(value=3),
                    })


