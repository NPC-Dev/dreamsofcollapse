from esper import esper
from component import position
from component import sprite
from component import rigidbody
from component import commandqueue
from component import movementstate
from component import repose
from component import clarity
from component import refreshment
from component import enemytypes
from component import stats
import pygame
import pymunk
from physics import physicsworld

DENSITY = 1.0
RADIUS = 6.0
WIDTH = 16
HEIGHT = 16
START_X = 104
START_Y = 104

BASE_REPOSE = 5
BASE_CLARITY = 1

class Player():

    def __init__(self, world, physicsWorld, xPos = START_X, yPos = START_Y, currTurn = 0):
        self.player = world.create_entity(
                                position.Position(x=xPos, y=yPos),
                                sprite.Sprite(sheet="sprites/saint11/mage-green.png", x=0, y=0, width=WIDTH, height=HEIGHT),
                                commandqueue.CommandQueue(waitForMe = True, initiative=currTurn),
                                movementstate.MovementState(pygame.Vector2(xPos, yPos)),
                                repose.Repose(baseVal=BASE_REPOSE),
                                clarity.Clarity(baseVal=BASE_CLARITY),
                                refreshment.Refreshment(baseVal=100, currVal=0),
                                enemytypes.EnemyTypes(myCategories=set(["player"]), enemyCategories=set(["monster"])),
                                stats.Stats(statsDict =
                                            {
                                                stats.StatType.DETERMINATION: stats.Stat(value=1),
                                                stats.StatType.CREATIVITY: stats.Stat(value=1),
                                                stats.StatType.WILLPOWER: stats.Stat(value=1),
                                                stats.StatType.WIT: stats.Stat(value=1),
                                                stats.StatType.LEVEL: stats.Stat(value=1),
                                                stats.StatType.POINTSAVAILABLE: stats.Stat(value=0),
                                            })
                            )
        
        body = rigidbody.EntityBody(self.player)
        body.position = (xPos, yPos)
        circle = pymunk.Circle(body, RADIUS)
        
        circle.filter = pymunk.ShapeFilter(
                                categories = physicsworld.Masks.GetCategory("player").categories,
                                mask = physicsworld.Masks.GetCategory("monster").categories) 
        
        circle.density = DENSITY
        circle.collision_type = physicsworld.Masks.GetCollisionType("player")

        physicsWorld.GetSpace().add(body, circle)
        
        rigidComponent = rigidbody.RigidBody(body, pygame.Vector2(0, 0))
        world.add_component(self.player, rigidComponent)

    def GetEntity(self):
        return self.player

