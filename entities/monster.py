from esper import esper
from component import position
from component import sprite
from component import rigidbody
from component import commandqueue
from component import movementstate
from component import repose
from component import enemytypes
from component import aistate
from component import stats
import pygame
import pymunk
from physics import physicsworld

DENSITY = 1.0
RADIUS = 6.0
WIDTH = 16
HEIGHT = 16
START_X = 88
START_Y = 88

BASE_REPOSE = 2

class Monster():

    def __init__(self, world, physicsWorld, xPos = START_X, yPos = START_Y, currTurn = 0):
        self.entity = world.create_entity(
                                position.Position(x=xPos, y=yPos),
                                self.GetSprite(),
                                commandqueue.CommandQueue(initiative=currTurn),
                                movementstate.MovementState(pygame.Vector2(xPos, yPos)),
                                enemytypes.EnemyTypes(myCategories=set(["monster"]), enemyCategories=set(["player"])),
                                repose.Repose(baseVal=BASE_REPOSE),
                                aistate.AIState(),
                                self.GetStats()
                            )
        
        body = rigidbody.EntityBody(self.entity)
        body.position = (xPos, yPos)
        circle = pymunk.Circle(body, RADIUS)
        circle.density = DENSITY
        circle.filter = pymunk.ShapeFilter(
                                categories = physicsworld.Masks.GetCategory("monster").categories, 
                                mask = physicsworld.Masks.GetCategory("player").categories | 
                                        physicsworld.Masks.GetCategory("monster").categories) 
        circle.collision_type = physicsworld.Masks.GetCollisionType("monster")

        physicsWorld.GetSpace().add(body, circle)
        
        rigidComponent = rigidbody.RigidBody(body, pygame.Vector2(0, 0))
        world.add_component(self.entity, rigidComponent)

    def GetSprite(self):
        return sprite.Sprite(sheet="sprites/joecreates/roguelikecreatures.png", x=16, y=0, width=WIDTH, height=HEIGHT)

    def GetStats(self):
        return stats.Stats(statsDict =
                    {
                        stats.StatType.DETERMINATION: stats.Stat(value=0),
                        stats.StatType.CREATIVITY: stats.Stat(value=0),
                        stats.StatType.WILLPOWER: stats.Stat(value=1),
                        stats.StatType.WIT: stats.Stat(value=1),
                        stats.StatType.LEVEL: stats.Stat(value=1),
                    })

    def GetEntity(self):
        return self.entity
