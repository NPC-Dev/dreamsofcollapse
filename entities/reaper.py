from entities import monster
from component import stats
from component import sprite

WIDTH=16
HEIGHT=16

class Reaper(monster.Monster):
    def GetSprite(self):
        return sprite.Sprite(sheet="sprites/joecreates/roguelikecreatures.png", x=80, y=96, width=WIDTH, height=HEIGHT)

    def GetStats(self):
        return stats.Stats(statsDict =
                    {
                        stats.StatType.DETERMINATION: stats.Stat(value=9),
                        stats.StatType.CREATIVITY: stats.Stat(value=0),
                        stats.StatType.WILLPOWER: stats.Stat(value=5),
                        stats.StatType.WIT: stats.Stat(value=7),
                        stats.StatType.LEVEL: stats.Stat(value=15),
                    })
