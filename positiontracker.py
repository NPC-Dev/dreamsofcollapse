import math
import pygame
from pubsub import pub
from groundcollapse.tileset import Pos

class PositionTracker:
    def __init__(self, tileHolder):
        pub.subscribe(self.OnPlayerSpawn, "player.spawn")
        pub.subscribe(self.OnEntityDied, "died")
        pub.subscribe(self.OnMonsterSpawn, "monster.spawn")
        pub.subscribe(self.OnEntityMoved, "entity.move")
        
        self.tileHolder = tileHolder
        self.tileSize = tileHolder.GetTileSize()

        self.positions = {}

    def WorldToTile(self, pos):
        return Pos(math.floor(pos.x/self.tileSize.x),
                    math.floor(pos.y/self.tileSize.y))

    def OnPlayerSpawn(self, player, pos):
        self.EntitySpawned(player.GetEntity(), self.WorldToTile(pos))
    
    def OnMonsterSpawn(self, monster, pos):
        self.EntitySpawned(monster.GetEntity(), self.WorldToTile(pos))

    def OnEntityDied(self, entity, pos, dueToDamage=False):
        self.EntityDied(entity, self.WorldToTile(pos))

    def OnEntityMoved(self, entity, oldPos, newPos):
        self.EntityMoved(entity, self.WorldToTile(oldPos), self.WorldToTile(newPos))
    
    def EntitySpawned(self, entity, pos):
        self.RegisterEntityAt(entity, pos)

    def EntityDied(self, entity, pos):
        self.UnregisterEntityAt(entity, pos)

    def EntityMoved(self, entity, oldPos, pos):
        self.UnregisterEntityAt(entity, oldPos)
        self.RegisterEntityAt(entity, pos)

    def UnregisterEntityAt(self, entity, pos):
        if not entity in self.positions.get(pos, set([])):
            print("Entity was not in the place we expected: " + str(entity) + " pos: " + str(pos))
            return
        self.positions[pos].remove(entity)

    def RegisterEntityAt(self, entity, pos):
        posSet = self.positions.get(pos, set([]))
        posSet.add(entity)
        self.positions[pos] = posSet

    def GetEntitySet(self, pos):
        return self.GetEntitySetAtTile(self.WorldToTile(pos))
    
    def GetEntitySetAtTile(self, pos):
        return self.positions.get(Pos(pos.x, pos.y), set([]))
    
    def IsAnyEntityAt(self, pos):
        return self.IsAnyEntityAtTile(self.WorldToTile(pos))
    
    def IsAnyEntityAtTile(self, pos):
        return len(self.GetEntitySetAtTile(pos)) > 0
