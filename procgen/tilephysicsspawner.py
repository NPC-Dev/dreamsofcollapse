from entities import tileentity
from pubsub import pub
from groundcollapse.tileset import Pos
import pygame

maxTimePerFrame = 5

class TilePhysicsSpawner:
    def __init__(self, tileset, world, physicsWorld):
        self.tileset = tileset
        self.world = world
        self.physicsWorld = physicsWorld
        self.tileSize = self.tileset.GetTileSize()
        pub.subscribe(self.OnTilesGenerated, "tilegen.created")
        pub.subscribe(self.OnTilesDestroyed, "tilegen.deleted")

        self.tileEntitiesToCreate = {}

    def ComputeWorldPos(self, chunkPos, x, y):
        tilePos = Pos((chunkPos.x + x) * self.tileSize.x, (chunkPos.y + y) * self.tileSize.y)
        return tilePos

    def OnTilesGenerated(self, chunkPos, tiles):
        for x in range(0, len(tiles)):
            for y in range (0, len(tiles[0])):
                tile = tiles[x][y]
                if tile == None:
                    continue

                tilePos = self.ComputeWorldPos(chunkPos, x, y)
                self.tileEntitiesToCreate[tilePos] = tile

    def Tick(self, frameStartTime, tickDiff):
        timeElapsed = pygame.time.get_ticks() - frameStartTime
        while timeElapsed < maxTimePerFrame and len(self.tileEntitiesToCreate) > 0:
            (tilePos, tile) = self.tileEntitiesToCreate.popitem()

            tileMetadata = self.tileset.GetTile(tile)
            if tileMetadata == None:
                print("Error: no metadata for tile: " + tile)
                continue
            
            tileEntity = tileentity.TileEntity(self.world, self.tileSize, self.physicsWorld, tileMetadata, tilePos.x, tilePos.y)
            timeElapsed = pygame.time.get_ticks() - frameStartTime

    def OnTilesDestroyed(self, chunkPos, tiles):
        for x in range(0, len(tiles)):
            for y in range (0, len(tiles[0])):
                tile = tiles[x][y]
                if tile == None:
                    continue

                tilePos = self.ComputeWorldPos(chunkPos, x, y)
                if tilePos in self.tileEntitiesToCreate:
                    del self.tileEntitiesToCreate[tilePos]
