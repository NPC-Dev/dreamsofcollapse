import pymunk
from physics import physicsworld
from pubsub import pub
from entities import entityutils

class ObjectDestroyer:
    def __init__(self, tileset, world, physicsWorld):
        self.tileset = tileset
        self.world = world
        self.physicsWorld = physicsWorld
        self.tileSize = self.tileset.GetTileSize()
        pub.subscribe(self.OnTilesDestroyed, "tilegen.deleted")
    
    def OnTilesDestroyed(self, chunkPos, tiles):
        left = chunkPos.x * self.tileSize.x
        bottom = chunkPos.y * self.tileSize.y
        
        if len(tiles) == 0:
            return

        right = left + len(tiles)*self.tileSize.x
        top = bottom + len(tiles[0])*self.tileSize.y
        
        box = pymunk.BB(left, bottom, right, top)
        
        shapeFilter = physicsworld.Masks.GetMask("player")
        
        queryResult = self.physicsWorld.GetSpace().bb_query(box, shapeFilter) 

        physicsToDelete = set([])
        for queryInfo in queryResult:
            body = queryInfo.body
            
            eventualPos = entityutils.GetEventualPosition(self.world, body.entity)
            pub.sendMessage("died", entity=body.entity, pos=eventualPos)

            self.world.delete_entity(body.entity) 
        
            physicsToDelete.add(body)
            physicsToDelete.update(body.shapes)

        if len(physicsToDelete) > 0:
            self.physicsWorld.GetSpace().remove(*physicsToDelete)
