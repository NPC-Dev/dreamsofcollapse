from pubsub import pub
from entities import monster
from entities import spider
from entities import skeleton
from entities import ghost
from entities import armoredskeleton
from entities import reaper
from groundcollapse.tileset import Pos
import pymunk
import pygame
from physics import physicsworld
import random
from component import stats

maxTimePerFrame = 5

MAX_LEVEL_DIFF = 3

class MonsterSpawner:
    def __init__(self, tileset, world, physicsWorld, player):
        self.tileset = tileset
        self.world = world
        self.physicsWorld = physicsWorld
        self.tileSize = self.tileset.GetTileSize()
        self.currTurn = 0
        self.player = player
        pub.subscribe(self.OnTilesGenerated, "tilegen.created")
        pub.subscribe(self.OnTilesDestroyed, "tilegen.deleted")
        pub.subscribe(self.OnTurnsPassed, "turns.passed")
        pub.subscribe(self.OnPlayerSpawn, "player.spawn")
        pub.subscribe(self.OnEntityDied, "died")

        self.toSpawn = {}
    
    def OnEntityDied(self, entity, pos, dueToDamage=False):
        if entity == self.player:
            self.player = None

    def OnPlayerSpawn(self, player, pos):
        self.player = player.GetEntity()


    def OnTurnsPassed(self, newTurn, oldTurn):
        self.currTurn = newTurn

    def ComputeWorldPos(self, chunkPos, x, y):
        tilePos = Pos((chunkPos.x + x) * self.tileSize.x, (chunkPos.y + y) * self.tileSize.y)
        return tilePos

    def OnTilesGenerated(self, chunkPos, tiles):
        for x in range(0, len(tiles)):
            for y in range (0, len(tiles[0])):
                tile = tiles[x][y]
                if tile == None:
                    continue

                tileMetadata = self.tileset.GetTile(tile)
                if tileMetadata == None:
                    print("Error: no metadata for tile: " + tile)
                    continue

                tileExtraData = tileMetadata.GetExtraData()
                if tileExtraData == None:
                    continue

                spawnData = tileExtraData.GetSpawnData()
                if spawnData == None:
                    continue

                spawnType = spawnData.GetSpawnType()
                if spawnType == None:
                    continue

                if spawnType != "enemy":
                    continue

                tilePos = self.ComputeWorldPos(chunkPos, x, y)

                self.toSpawn[tilePos] = spawnType

    def Tick(self, frameStartTime, tickDiff):
        playerLevel = 1
        if self.player != None:
            statsComp = self.world.try_component(self.player, stats.Stats)
            if statsComp:
                playerLevelStat = statsComp.stats.get(stats.StatType.LEVEL, None)
                if playerLevelStat != None:
                    playerLevel = playerLevelStat.GetValue()

        timeElapsed = pygame.time.get_ticks() - frameStartTime
        while timeElapsed < maxTimePerFrame and len(self.toSpawn) > 0:
            (tilePos, spawnInfo) = self.toSpawn.popitem()
            
            monsterPos = pygame.Vector2(tilePos.x+self.tileSize.x/2, tilePos.y+self.tileSize.y/2)
            
            monsterType = self.DetermineMonsterByLevel(playerLevel)
            
            monsterID = monsterType(self.world, self.physicsWorld, monsterPos.x, monsterPos.y, self.currTurn)
    
            pub.sendMessage("monster.spawn", monster=monsterID, pos=monsterPos)

            timeElapsed = pygame.time.get_ticks() - frameStartTime
    
    def DetermineMonsterByLevel(self, playerLevel):
            monstersByLevel = [monster.Monster,
                                monster.Monster,
                                monster.Monster,
                                spider.Spider,
                                spider.Spider,
                                spider.Spider,
                                skeleton.Skeleton,
                                skeleton.Skeleton,
                                skeleton.Skeleton,
                                skeleton.Skeleton,
                                ghost.Ghost,
                                ghost.Ghost,
                                ghost.Ghost,
                                armoredskeleton.ArmoredSkeleton,
                                armoredskeleton.ArmoredSkeleton,
                                armoredskeleton.ArmoredSkeleton,
                                reaper.Reaper,
                                reaper.Reaper,
                                reaper.Reaper,
                                reaper.Reaper]

            selectedOffset = random.randrange(-MAX_LEVEL_DIFF, MAX_LEVEL_DIFF)
            selected = min(len(monstersByLevel)-1, max(0, (playerLevel-1) + selectedOffset))

            return monstersByLevel[selected]
    
    def OnTilesDestroyed(self, chunkPos, tiles):
        for x in range(0, len(tiles)):
            for y in range (0, len(tiles[0])):
                tile = tiles[x][y]
                if tile == None:
                    continue

                tilePos = self.ComputeWorldPos(chunkPos, x, y)
                if tilePos in self.toSpawn:
                    del self.toSpawn[tilePos]
