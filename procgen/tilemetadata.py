import pandas

class SpawnData:
    def __init__(self, rawSpawnData):
        self.spawnType = None
        if "spawntype" in rawSpawnData:
            self.spawnType = rawSpawnData["spawntype"]

    def GetSpawnType(self):
        return self.spawnType

class TileMetadata:
    def __init__(self, rawMetadata):
        self.spawnData = None
        if "spawninfo" in rawMetadata:
            self.spawnData = SpawnData(rawMetadata["spawninfo"])

    def GetSpawnData(self):
        return self.spawnData
